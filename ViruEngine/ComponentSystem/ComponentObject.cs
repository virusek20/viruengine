﻿using System;
using System.Collections.Generic;

namespace ViruEngine.ComponentSystem
{
    public class ComponentObject<T>
    {
        public Dictionary<Type, T> ComponentList = new Dictionary<Type, T>();
        internal ViruGame Game;

        public TY GetComponent<TY>() where TY : class, T
        {
            T component;
            ComponentList.TryGetValue(typeof (TY), out component);
            return component as TY;
        }

        public bool HasComponent<TY>() where TY : class, T
        {
            T component;
            return ComponentList.TryGetValue(typeof (TY), out component);
        }

        public void RemoveComponent<TY>() where TY : class, T
        {
            if (!HasComponent<TY>()) return;

            ComponentList.Remove(typeof (TY));
        }

        public TY AddComponent<TY>() where TY : class, T
        {
            if (HasComponent<TY>()) return null;

            var component = Activator.CreateInstance(typeof (TY), Game, this) as TY;
            if (component == null) return null;

            ComponentList.Add(typeof (TY), component);
            return component;
        }
    }
}