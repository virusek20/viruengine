﻿using System;
using System.IO;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace ViruEngine.ComponentSystem
{
    public class Component<T> where T : ComponentObject<Component<T>>
    {
        protected internal readonly ViruGame Game;
        protected internal readonly T Parent;

        protected Component(ViruGame game, T parent)
        {
            Game = game;
            Parent = parent;

            if (!CheckAvailability(Parent)) throw new Exception("Missing component in entity!");
        }

        public virtual bool CheckAvailability(T gameEntity)
        {
            return true;
        }

        public virtual void Init(GameTime gameTime)
        {
        }

        public virtual void Deinit(GameTime gameTime)
        {
        }

        public virtual int GetSaveSize()
        {
            return 0;
        }

        public virtual void Save(BinaryWriter writer)
        {
            writer.Write(GetType().Name);
            writer.Write(GetSaveSize());
        }

        public virtual void Load(BinaryReader reader)
        {
        }

        public virtual void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
        }

        public virtual void Update(GameTime gameTime)
        {
        }
    }
}