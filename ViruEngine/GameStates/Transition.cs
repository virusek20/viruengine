﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace ViruEngine.GameStates
{
    public class Transition : GameState
    {
        private Texture2D _curTexture;
        private GameState _destState;
        private Texture2D _destTexture;
        private int _fraction;

        public void DoTransition(Texture2D currentBckg, string destBckg, GameState desinationState)
        {
            Game.ActiveState = Game.TransitionState;

            _curTexture = currentBckg;
            _destTexture = Game.Content.Load<Texture2D>(destBckg);
            _destState = desinationState;
            _fraction = 0;
        }

        public override void Update(GameTime gameTime)
        {
            _fraction += 5;
            if (_fraction > 255)
            {
                _curTexture = null;
                _destTexture = null;
                _fraction = 0;

                Game.ActiveState = _destState;
            }
        }

        public override void Draw(GameTime gameTime)
        {
            Game.SpriteBatch.Begin();
            Game.SpriteBatch.Draw(_curTexture, new Vector2(0, 0), null, new Color(255, 255, 255, 255 - _fraction), 0,
                new Vector2(0, 0),
                new Vector2(Game.Graphics.PreferredBackBufferWidth/1920f,
                    Game.Graphics.PreferredBackBufferHeight/1080f), SpriteEffects.None, 0);
            Game.SpriteBatch.Draw(_destTexture, new Vector2(0, 0), null, new Color(255, 255, 255, _fraction), 0,
                new Vector2(0, 0),
                new Vector2(Game.Graphics.PreferredBackBufferWidth/1920f,
                    Game.Graphics.PreferredBackBufferHeight/1080f), SpriteEffects.None, 0);

            Game.SpriteBatch.End();
        }
    }
}