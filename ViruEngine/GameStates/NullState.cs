﻿using System.Diagnostics;
using System.Reflection;
using Microsoft.Xna.Framework;

namespace ViruEngine.GameStates
{
    internal class NullState : GameState
    {
        private Process _selfProcess;

        public override void Initialize(ViruGame game)
        {
            base.Initialize(game);
            _selfProcess = Process.GetCurrentProcess();

            Loaded = true;
        }

        public override void UnloadContent()
        {
            base.UnloadContent();
            _selfProcess.Dispose();
        }

        public override void Draw(GameTime gameTime)
        {
            Game.GraphicsDevice.Clear(Color.DarkBlue);

            Game.SpriteBatch.Begin();

            Game.SpriteBatch.DrawString(Game.BasicFont, "Active gamestate: Null",
                new Vector2(
                    Game.Graphics.PreferredBackBufferWidth - 5 -
                    Game.BasicFont.MeasureString("Active gamestate: Null").X, 1), Color.White);
            Game.SpriteBatch.DrawString(Game.BasicFont,
                Game.GameName + " v" + Assembly.GetExecutingAssembly().GetName().Version, new Vector2(5, 1), Color.White);
            Game.SpriteBatch.DrawString(Game.BasicFont,
                "Resolution: " + Game.Graphics.PreferredBackBufferWidth + "x" + Game.Graphics.PreferredBackBufferHeight,
                new Vector2(5, 21), Color.White);
            Game.SpriteBatch.DrawString(Game.BasicFont,
                Game.GraphicsDevice.Adapter.Description + " : " + Game.GraphicsDevice.Adapter.DeviceName,
                new Vector2(5, 41), Color.White);
            Game.SpriteBatch.DrawString(Game.BasicFont,
                string.Format("Memory usage: {0}MB {1}KB", _selfProcess.PrivateMemorySize64/1024/1024,
                    _selfProcess.PrivateMemorySize64/1024%1024), new Vector2(5, 61), Color.White);

            Game.SpriteBatch.End();
        }
    }
}