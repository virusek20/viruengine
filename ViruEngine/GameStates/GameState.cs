﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using ViruEngine.GUI;

namespace ViruEngine.GameStates
{
    public class GameState
    {
        public delegate void GuiDisableEventHandler(int priority);

        public delegate void GuiDrawEventHandler(GameTime gameTime, SpriteBatch spriteBatch);

        public delegate void GuiEnableEventHandler(int priority);

        public delegate void GuiHideEventHandler(int priority, HiddenState setState);

        public delegate void GuiCheckEventHandler(GameTime gameTime, Point point, bool click);

        public delegate void GuiResizeEventHandler();

        public bool ContentLoaded;
        protected ViruGame Game;
        public bool Loaded;

        protected GameState()
        {
        }

        public event GuiCheckEventHandler OnCheck;
        public event GuiEnableEventHandler OnEnable;
        public event GuiDisableEventHandler OnDisable;
        public event GuiResizeEventHandler OnResize;
        public event GuiDrawEventHandler OnDraw;
        public event GuiHideEventHandler OnHide;

        public virtual void DoEnable(int priority)
        {
            var handler = OnEnable;
            if (handler != null) handler(priority);
        }

        public virtual void DoDisable(int priority)
        {
            var handler = OnDisable;
            if (handler != null) handler(priority);
        }

        protected virtual void DoCheck(GameTime gameTime, Point point, bool click)
        {
            var handler = OnCheck;
            if (handler != null) handler(gameTime, point, click);
        }

        protected virtual void DoDraw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            var handler = OnDraw;
            if (handler != null) handler(gameTime, spriteBatch);
        }

        protected virtual void DoResize()
        {
            var handler = OnResize;
            if (handler != null) handler();
        }

        public virtual void DoHide(int priority, HiddenState setstate)
        {
            var handler = OnHide;
            if (handler != null) handler(priority, setstate);
        }

        public virtual void Initialize(ViruGame game)
        {
            Game = game;
        }

        public virtual void LoadContent(ContentManager content)
        {
        }

        public virtual void UnloadContent()
        {
        }

        public virtual void Update(GameTime gameTime)
        {
        }

        public virtual void Draw(GameTime gameTime)
        {
        }

        public virtual void RecalculateSizes()
        {
        }
    }
}