﻿using System;
using Microsoft.Xna.Framework;

namespace ViruEngine.Graphics.Colors
{
    public static class ColorRandomizer
    {
        private static readonly Random Random = new Random();

        public static Color GetRandomColor()
        {
            var color = new Color(Random.Next(255), Random.Next(255), Random.Next(255));

            return color;
        }
    }
}