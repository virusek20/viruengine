﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using ViruEngine.List;

namespace ViruEngine.Graphics.Textures
{
    public class ComposedTexture
    {
        public static Vector2 BaseOffset = new Vector2(32, 32);
        internal static ViruGame Game;
        public readonly Texture2D Texture;
        public Vector2 Offset = BaseOffset;
        public OrderedHashSet<int> IncludedFrames = new OrderedHashSet<int>(); 
        public string TextureFilename = "XNA_CONTENT";
        public float Rotation = 0f;

        [EngineLoad(true, LoadTime.Init)]
        public static void Initialize(ViruGame game)
        {
            Game = game;
        }

        public ComposedTexture(string textureFileName, Vector2 offset)
        {
            TextureFilename = textureFileName;
            Texture = ImageTexture.FromImage(Game.Content.RootDirectory + TextureFilename);
            Offset = offset;
        }

        public ComposedTexture(Texture2D texture, Vector2 offset)
        {
            Texture = texture;
            Offset = offset;
        }

        public ComposedTexture(Texture2D texture)
        {
            Texture = texture;
        }

        public static implicit operator ComposedTexture(Texture2D texture)
        {
            return new ComposedTexture(texture);
        }


        public void Draw(Vector2 position)
        {
            foreach (var frameNumber in IncludedFrames)
            {
                Game.SpriteBatch.Draw(Texture, new Rectangle((int) (position.X + Offset.X / 2), (int) (position.Y + Offset.Y / 2), (int) Offset.X, (int) Offset.Y), new Rectangle((int)(frameNumber * Offset.X % Texture.Width), (int)((int)(frameNumber * Offset.X) / Texture.Width * Offset.Y), (int)Offset.X, (int)Offset.Y) ,Color.White, Rotation, Offset / 2, SpriteEffects.None, 0);
            }
        }
    }
}
