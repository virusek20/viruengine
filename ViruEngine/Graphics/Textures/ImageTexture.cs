﻿using System.Drawing;
using Microsoft.Xna.Framework.Graphics;
using Color = Microsoft.Xna.Framework.Color;

namespace ViruEngine.Graphics.Textures
{
    public static class ImageTexture
    {
        internal static ViruGame Game;

        [EngineLoad(true, LoadTime.PreInit)]
        public static void Initialize(ViruGame game)
        {
            Game = game;
        }

        public static void InvertColors(ref Texture2D texture)
        {
            var data = new Color[texture.Width*texture.Height];
            texture.GetData(data);

            for (var i = 0; i < data.Length; i++)
            {
                data[i] = new Color(255 - data[i].R, 255 - data[i].G, 255 - data[i].B, data[i].A);
            }

            texture.SetData(data);
        }

        public static Texture2D FromImage(string fileName)
        {
            var bitmap = new Bitmap(Game.Content.RootDirectory + "/" + fileName);

            var texture = new Texture2D(Game.GraphicsDevice, bitmap.Width, bitmap.Height);
            var colorData = new Color[texture.Width*texture.Height];
            var i = 0;

            for (var y = 0; y < bitmap.Height; y++)
            {
                for (var x = 0; x < bitmap.Width; x++)
                {
                    var pixel = bitmap.GetPixel(x, y);
                    colorData[i] = new Color(pixel.R, pixel.G, pixel.B, pixel.A);

                    i++;
                }
            }

            texture.SetData(colorData);

            bitmap.Dispose();

            return texture;
        }
    }
}