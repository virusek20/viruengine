﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace ViruEngine.Graphics.Textures
{
    public class AnimatedTexture
    {
        public static Vector2 BaseOffset = new Vector2(128, 128);
        internal static ViruGame Game;
        public readonly Texture2D Texture;
        private int _frameNumber;
        private Vector2 _position = Vector2.Zero;
        public Vector2 Offset;
        public Vector2 Shift = Vector2.Zero;
        public string TextureFilename = "XNA_CONTENT";
        public float Rotation;
        public Vector2 Origin;
        public Color Tint = Color.White;

        [EngineLoad(true, LoadTime.Init)]
        public static void Initialize(ViruGame game)
        {
            Game = game;
        }

        public AnimatedTexture(string textureFileName, Vector2 offset)
        {
            TextureFilename = textureFileName;
            Texture = ImageTexture.FromImage(Game.Content.RootDirectory + TextureFilename);
            Offset = offset;

            Origin = Offset / 2;
        }

        public AnimatedTexture(Texture2D texture, Vector2 offset)
        {
            Texture = texture;
            Offset = offset;

            Origin = Offset / 2;
        }

        public AnimatedTexture(Texture2D texture)
        {
            Texture = texture;
            Offset = BaseOffset;

            Origin = Offset / 2;
        }

        public int FrameNumber
        {
            get { return _frameNumber; }
            set
            {
                _frameNumber = value;
                _position.X = _frameNumber*Offset.X%Texture.Width;
                _position.Y = (int) (_frameNumber*Offset.X)/Texture.Width*Offset.Y;
            }
        }

        public int Frames
        {
            get
            {
                return (int)(Texture.Width / Offset.X) * (int)(Texture.Height / Offset.Y);
            }
        }

        public static implicit operator AnimatedTexture(Texture2D texture)
        {
            return new AnimatedTexture(texture);
        }

        public void NextFrame()
        {
            FrameNumber++;
            if (_position.Y >= Texture.Height)
            {
                FrameNumber = 0;
            }
        }

        public AnimatedTexture Clone()
        {
            AnimatedTexture clone = new AnimatedTexture(Texture, Offset)
            {
                Shift = Shift,
                FrameNumber = FrameNumber,
                Rotation = Rotation,
                TextureFilename = TextureFilename,
                Origin = Origin,
                Tint = Tint
            };

            return clone;
        }

        public void Draw(Vector2 position)
        {
            Game.SpriteBatch.Draw(Texture, new Rectangle((int) (position.X + Origin.X), (int) (position.Y + Origin.Y), (int)Offset.X, (int)Offset.Y), 
                new Rectangle((int) (_position.X + Shift.X), (int) (_position.Y + Shift.Y), (int) Offset.X, (int) Offset.Y),
                Tint, Rotation, Origin, SpriteEffects.None, 0);

        }
    }
}