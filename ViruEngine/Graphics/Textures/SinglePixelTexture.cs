﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace ViruEngine.Graphics.Textures
{
    public static class SinglePixelTexture
    {
        private static ViruGame Game;

        [EngineLoad(true, LoadTime.PreInit)]
        public static void Initialize(ViruGame game)
        {
            Game = game;
        }

        public static Texture2D GetSinglePixelTexture(Color color)
        {
            var texture = new Texture2D(Game.GraphicsDevice, 1, 1);
            texture.SetData(new[] {color});

            return texture;
        }
    }
}