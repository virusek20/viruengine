﻿using System;
using System.IO;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace ViruEngine.Graphics.Rendering
{
    public class RenderablePolygon
    {
        public static BasicEffect BasicEffect;
        public Color Color = Color.Red;
        public Vector2 Origin;
        public float Rotation;
        public bool Tri;
        public Vector2[] Vertices;
        public Color[] Colors;

        public RenderablePolygon(ViruGame game, string fileName)
        {
            using (
                BinaryReader reader =
                    new BinaryReader(new FileStream(game.Content.RootDirectory + "/" + fileName, FileMode.Open)))
            {
                Tri = reader.ReadBoolean();
                var fullColor = reader.ReadBoolean();

                if (!fullColor)
                {
                    byte a = reader.ReadByte();
                    byte r = reader.ReadByte();
                    byte g = reader.ReadByte();
                    byte b = reader.ReadByte();

                    Color = new Color(r, g, b, a);
                }

                Vertices = new Vector2[reader.ReadInt32()];
                Colors = new Color[Vertices.Length];

                for (int i = 0; i < Vertices.Length; i++)
                {
                    Vertices[i] = new Vector2(reader.ReadInt32(), reader.ReadInt32());
                    if (fullColor)
                    {
                        byte a = reader.ReadByte();
                        byte r = reader.ReadByte();
                        byte g = reader.ReadByte();
                        byte b = reader.ReadByte();

                        Colors[i] = new Color(r, g, b, a);
                    }
                }
            }
        }

        public RenderablePolygon(Rectangle rectangle, float rotation, bool tri = false)
        {
            if (tri)
            {
                Vertices = new Vector2[5];
                Colors = new Color[5];

                Vertices[0] = new Vector2(rectangle.X, rectangle.Y);
                Vertices[1] = new Vector2(rectangle.X + rectangle.Width, rectangle.Y);
                Vertices[2] = new Vector2(rectangle.X, rectangle.Y + rectangle.Height);
                Vertices[3] = new Vector2(rectangle.X + rectangle.Width, rectangle.Y + rectangle.Height);
                Vertices[4] = new Vector2(rectangle.X + rectangle.Width, rectangle.Y);
            }
            else
            {
                Vertices = new Vector2[4];
                Colors = new Color[4];

                Vertices[0] = new Vector2(rectangle.X, rectangle.Y);
                Vertices[1] = new Vector2(rectangle.X + rectangle.Width, rectangle.Y);
                Vertices[2] = new Vector2(rectangle.X + rectangle.Width, rectangle.Y + rectangle.Height);
                Vertices[3] = new Vector2(rectangle.X, rectangle.Y + rectangle.Height);
            }

            Tri = tri;

            Origin = new Vector2(rectangle.Width/2f + rectangle.X, rectangle.Height/2f + rectangle.Y);

            Rotation = rotation;
        }

        public RenderablePolygon(Vector2[] vertex, bool tri = false)
        {
            Vertices = vertex;
            Colors = new Color[Vertices.Length];
            Tri = tri;

            var total = Vector2.Zero;

            for (var i = 0; i < vertex.Length; i++)
            {
                total += vertex[i];
            }

            Origin = total/vertex.Length;
        }

        public int X
        {
            get
            {
                var vertices = Vector2ToVertexPositionColor(Vertices, true, true);

                var lowest = float.MaxValue;

                for (var i = 0; i < Vertices.Length; i++)
                {
                    if (vertices[i].Position.X < lowest) lowest = vertices[i].Position.X;
                }

                return (int) lowest;
            }
            set { Move(new Vector2(value, Vertices[0].Y)); }
        }

        public int Y
        {
            get
            {
                var vertices = Vector2ToVertexPositionColor(Vertices, true, true);

                var highest = float.MinValue;

                for (var i = 0; i < Vertices.Length; i++)
                {
                    if (vertices[i].Position.Y > highest) highest = vertices[i].Position.Y;
                }

                return (int) highest;
            }
            set { Move(new Vector2(Vertices[0].X, value)); }
        }

        public Vector2 TopLeft
        {
            get
            {
                var vertices = Vector2ToVertexPositionColor(Vertices, true, true);

                var lowestX = float.MaxValue;
                var lowestY = float.MaxValue;

                for (var i = 0; i < Vertices.Length; i++)
                {
                    if (vertices[i].Position.X < lowestX) lowestX = vertices[i].Position.X;
                    if (vertices[i].Position.Y < lowestY) lowestY = vertices[i].Position.Y;
                }

                return new Vector2(lowestX, lowestY);
            }
        }

        public int Width
        {
            get
            {
                var vertices = Vector2ToVertexPositionColor(Vertices, true, true);

                var lowest = float.MaxValue;
                var highest = float.MinValue;

                for (var i = 0; i < vertices.Length; i++)
                {
                    if (vertices[i].Position.X < lowest) lowest = vertices[i].Position.X;
                    if (vertices[i].Position.X > highest) highest = vertices[i].Position.X;
                }

                return (int) (highest - lowest);
            }
        }

        public int Height
        {
            get
            {
                var vertices = Vector2ToVertexPositionColor(Vertices, true, true);

                var lowest = float.MaxValue;
                var highest = float.MinValue;

                for (var i = 0; i < vertices.Length; i++)
                {
                    if (vertices[i].Position.Y < lowest) lowest = vertices[i].Position.Y;
                    if (vertices[i].Position.Y > highest) highest = vertices[i].Position.Y;
                }

                return (int) (highest - lowest);
            }
        }

        [EngineLoad(true, LoadTime.Init)]
        public static void Initialize(ViruGame game)
        {
            BasicEffect = new BasicEffect(game.Graphics.GraphicsDevice)
            {
                VertexColorEnabled = true,
                Projection =
                    Matrix.CreateOrthographicOffCenter(0, game.Graphics.GraphicsDevice.Viewport.Width,
                        game.Graphics.GraphicsDevice.Viewport.Height, 0, 0, 1)
            };
        }

        public Vector2[] GetAxes()
        {
            var vertices = Vector2ToVertexPositionColor(Vertices, true, true);
            var axes = new Vector2[vertices.Length];

            for (var i = 0; i < vertices.Length; i++)
            {
                Vector3 axis;
                if (i + 1 == vertices.Length) axis = vertices[i].Position - vertices[0].Position;
                else axis = vertices[i].Position - vertices[i + 1].Position;

                var temp = axis.X;

                axis.X = axis.Y;
                axis.Y = temp;

                axis.X = -axis.X;
                axes[i] = new Vector2(axis.X, axis.Y)/100;
            }

            return axes;
        }

        public Vector2 Project(Vector2 axis)
        {
            var vertices = Vector2ToVertexPositionColor(Vertices, true, true);

            var min = Vector2.Dot(axis, new Vector2(vertices[0].Position.X, vertices[0].Position.Y));
            var max = min;

            for (var i = 1; i < vertices.Length; i++)
            {
                var p = Vector2.Dot(axis, new Vector2(vertices[i].Position.X, vertices[i].Position.Y));
                if (p < min)
                {
                    min = p;
                }
                else if (p > max)
                {
                    max = p;
                }
            }
            return new Vector2(min, max);
        }

        public bool Intersects(RenderablePolygon shape)
        {
            var shape1Axes = GetAxes();
            var shape2Axes = shape.GetAxes();

            for (var i = 0; i < shape1Axes.Length; i++)
            {
                var projction1 = Project(shape1Axes[i]);
                var projction2 = shape.Project(shape1Axes[i]);

                var check = true;

                if (projction1.X <= projction2.Y & projction1.Y >= projction2.Y) check = true;
                else if (projction2.X <= projction1.Y & projction2.Y >= projction1.Y) check = true;
                else check = false;

                if (!check) return false;
            }

            for (var i = 0; i < shape2Axes.Length; i++)
            {
                var projction1 = Project(shape2Axes[i]);
                var projction2 = shape.Project(shape2Axes[i]);

                var check = true;

                if (projction1.X <= projction2.Y & projction1.Y >= projction2.Y) check = true;
                else if (projction2.X <= projction1.Y & projction2.Y >= projction1.Y) check = true;
                else check = false;

                if (!check) return false;
            }

            return true;
        }

        public void Move(Vector2 position)
        {
            var offsets = new Vector2[Vertices.Length];
            var originOffset = Origin - Vertices[0];

            Origin = position + originOffset;

            for (var i = 1; i < Vertices.Length; i++)
            {
                offsets[i] = Vertices[i] - Vertices[i - 1];
            }

            offsets[0] = Vector2.Zero;
            Vertices[0] = position;

            for (var i = 0; i < Vertices.Length; i++)
            {
                Vertices[i] = position;
                for (var j = 0; j <= i; j++)
                {
                    Vertices[i] += offsets[j];
                }
            }
        }

        public void Translate(Vector2 translationVector)
        {
            for (var i = 0; i < Vertices.Length; i++)
            {
                Vertices[i] += translationVector;
            }

            Origin += translationVector;
        }

        public VertexPositionColor[] GetRotated(VertexPositionColor[] vertices)
        {
            for (var i = 0; i < vertices.Length; i++)
            {
                var translationX = vertices[i].Position.X - Origin.X;
                var translationY = vertices[i].Position.Y - Origin.Y;

                vertices[i].Position.X =
                    (float) (translationX*Math.Cos(Rotation) + translationY*Math.Sin(Rotation) + Origin.X);
                vertices[i].Position.Y =
                    (float) (-translationX*Math.Sin(Rotation) + translationY*Math.Cos(Rotation) + Origin.Y);
            }

            return vertices;
        }

        public VertexPositionColor[] Vector2ToVertexPositionColor(Vector2[] vertices, bool close, bool rotate)
        {
            VertexPositionColor[] closedShape;

            if (Tri)
            {
                closedShape = new VertexPositionColor[Vertices.Length];
            }
            else if (close)
            {
                closedShape = new VertexPositionColor[Vertices.Length + 1];
            }
            else
            {
                closedShape = new VertexPositionColor[Vertices.Length];
            }

            for (var index = 0; index < Vertices.Length; index++)
            {
                Color color = Colors[index];
                if (color == Color.Transparent) color = Color;
                
                closedShape[index] = new VertexPositionColor(new Vector3(vertices[index], 0), color);
            }

            if (!Tri & close)
            {
                Color color = Colors[0];
                if (color == Color.Transparent) color = Color;
                closedShape[vertices.Length] = new VertexPositionColor(new Vector3(vertices[0], 0), color);
            }

            if (rotate) GetRotated(closedShape);

            return closedShape;
        }

        public void Draw(GraphicsDevice device)
        {
            var closedShape = Vector2ToVertexPositionColor(Vertices, true, true);

            for (int i = 0; i < BasicEffect.CurrentTechnique.Passes.Count; i++)
            {
                BasicEffect.CurrentTechnique.Passes[i].Apply();
            }

            if (Tri)
            {
                device.RasterizerState = RasterizerState.CullNone;
                device.DrawUserPrimitives(PrimitiveType.TriangleList, closedShape, 0, closedShape.Length/3);
            }
            else
            {
                device.DrawUserPrimitives(PrimitiveType.LineStrip, closedShape, 0, closedShape.Length);
            }
        }
    }
}