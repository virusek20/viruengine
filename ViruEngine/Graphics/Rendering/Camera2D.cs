﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace ViruEngine.Graphics.Rendering
{
    public class Camera2D
    {
        internal readonly ViruGame Game;
        private float _zoom;
        public bool CanvasMode;
        public bool Centered;
        public Matrix Transform;

        public Camera2D(ViruGame game)
        {
            Game = game;
            _zoom = 1.0f;
            Rotation = 0.0f;
            Pos = Vector2.Zero;
        }

        public float Zoom
        {
            get { return _zoom; }
            set
            {
                _zoom = value;
                if (_zoom < 0.4f) _zoom = 0.4f;
                if (_zoom > 3f) _zoom = 3f;
            }
        }

        public float Rotation { get; set; }
        public Vector2 Pos { get; set; }

        public void Move(Vector2 amount)
        {
            Pos += amount;
        }

        public Matrix GetTransformation(GraphicsDevice graphicsDevice)
        {
            Transform =
                Matrix.CreateRotationZ(Rotation)*
                Matrix.CreateScale(new Vector3(Zoom, Zoom, 1));

            if (Centered)
            {
                Transform *=
                    Matrix.CreateTranslation(new Vector3(Game.Graphics.PreferredBackBufferWidth*0.5f,
                        Game.Graphics.PreferredBackBufferHeight*0.5f, 0));
            }

            if (CanvasMode)
            {
                Transform *= Matrix.CreateTranslation(new Vector3(Pos.X, Pos.Y, 0));
            }
            else
            {
                Transform *= Matrix.CreateTranslation(new Vector3(-Pos.X, -Pos.Y, 0));
            }

            return Transform;
        }
    }
}