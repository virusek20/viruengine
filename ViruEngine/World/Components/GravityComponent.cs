﻿using ViruEngine.ComponentSystem;

namespace ViruEngine.World.Components
{
    internal class GravityComponent : Component<GameLevel>
    {
        public float Gravity = 1;

        public GravityComponent(ViruGame game, GameLevel parent) : base(game, parent)
        {
        }
    }
}