﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Reflection;
using Microsoft.Xna.Framework;
using ViruEngine.ComponentSystem;
using ViruEngine.Console;
using ViruEngine.Entities;
using ViruEngine.Graphics.Textures;
using Color = Microsoft.Xna.Framework.Color;

namespace ViruEngine.World
{
    public class GameLevel : ComponentObject<Component<GameLevel>>
    {
        public static string ComponentsNamespace;
        public AnimatedTexture Background;
        public List<GameEntity> Entities = new List<GameEntity>();
        public GameEntity PlayerEntity;
        public int Height;
        public string LevelInformation = "";
        public string Name = "";
        public Bitmap Thumbnail;
        public int Width;
        private int _updateLevel;

        public GameLevel(ViruGame game, string filename)
        {
            Game = game;

            using (var reader = new BinaryReader(new FileStream(filename, FileMode.Open)))
            {
                Name = reader.ReadString();
                LevelInformation = reader.ReadString();
                Width = reader.ReadInt32();
                Height = reader.ReadInt32();

                while (reader.BaseStream.Length != reader.BaseStream.Position)
                {
                    var entityID = reader.ReadString();
                    var entitySpawn = reader.ReadBoolean();
                    var entity = EntityManager.BuildEntity(entityID, Game, this);

                    for (var i = 0; i < entity.ComponentList.Count; i++)
                    {
                        var type = reader.ReadString();

                        Type componentType = null;

                        if (ComponentsNamespace != null)
                        {
                            componentType =
                                Assembly.GetEntryAssembly()
                                    .GetType(Assembly.GetEntryAssembly().GetName().Name + "." + ComponentsNamespace +
                                             "." + type);
                        }
                        else
                        {
                            GameConsole.WriteLine("No defualt GameEntity component namespace defined!", Color.Red);
                        }

                        if (componentType == null)
                        {
                            componentType =
                                Type.GetType(Assembly.GetExecutingAssembly().GetName().Name + ".Entities.Components." +
                                             type);
                        }

                        if (componentType == null)
                        {
                            throw new Exception("Component " + type + " not found");
                        }

                        var component =
                            typeof (GameEntity).GetMethod("GetComponent")
                                .MakeGenericMethod(componentType)
                                .Invoke(entity, null) as Component<GameEntity>;
                        if (component == null)
                            throw new Exception(entity.ID + ": Failed to create all components! (" + componentType.Name +
                                                ")");

                        component.Load(reader);
                    }

                    if (entitySpawn) entity.SetSpawnedState(null, true);

                    AddEntity(entity);
                }
            }
        }

        public GameLevel(ViruGame game, int width, int height)
        {
            Game = game;

            Width = width;
            Height = height;
        }

        public void Init(ViruGame game, GameTime gameTime)
        {
            foreach (var component in ComponentList.Values)
            {
                component.Init(gameTime);
            }

            foreach (var t in Entities)
            {
                t.Init(gameTime);
            }
        }

        public void Save(string filename)
        {
            using (var writer = new BinaryWriter(new FileStream(filename, FileMode.Create)))
            {
                writer.Write(Name);
                writer.Write(LevelInformation);
                writer.Write(Width);
                writer.Write(Height);

                foreach (var entity in Entities)
                {
                    writer.Write(entity.ID);
                    writer.Write(entity.GetSpawnedState());

                    foreach (var component in entity.ComponentList.Values)
                    {
                        component.Save(writer);
                    }
                }
            }
        }

        public void Deinit(GameTime gameTime)
        {
            foreach (var component in ComponentList.Values)
            {
                component.Deinit(gameTime);
            }
        }

        public void AddEntity(GameEntity entity)
        {
            if (entity == null) return;
            entity.Init(null);
            Entities.Add(entity);

            if (entity.UpdateOrder > _updateLevel) _updateLevel = entity.UpdateOrder + 1;
        }

        public void Draw(GameTime gameTime)
        {
            foreach (var t in ComponentList.Values)
            {
                t.Draw(gameTime, Game.SpriteBatch);
            }

            for (int i = 0; i < _updateLevel; i++)
            {
                foreach (var t in Entities)
                {
                    if (t.UpdateOrder == i) t.Draw(gameTime, Game.SpriteBatch);
                }
            }
        }

        public void DrawBackground(GameTime gameTime)
        {
            if (Background != null) Background.Draw(Vector2.Zero);
        }

        public void Update(GameTime gameTime)
        {
            foreach (var component in ComponentList.Values)
            {
                component.Update(gameTime);
            }

            for (int i = 0; i < _updateLevel; i++)
            {
                foreach (var t in Entities)
                {
                    if (t.UpdateOrder == i) t.Update(gameTime);
                }
            }
        }
    }
}