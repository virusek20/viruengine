using System.Collections.Generic;
using System.Linq.Expressions;
using Microsoft.Xna.Framework;

namespace ViruEngine.Physics
{
    public class Polygon
    {
        private readonly List<Vector> _edges = new List<Vector>();
        private readonly List<Vector> _points = new List<Vector>();

        public float X
        {
            get
            {
                if (_points.Count == 0) return 0f;
                return _points[0].X;
            }
            set
            {
                if (_points.Count == 0) return;

                var distance = value - _points[0].X;

                _points[0] = new Vector(value, _points[0].Y);


                for (var index = 0; index < _points.Count; index++)
                {
                    if (index == 0) continue;

                    _points[index] = new Vector(_points[index].X + distance, _points[index].Y);
                }

                BuildEdges();
            }
        }

        public float Y
        {
            get
            {
                if (_points.Count == 0) return 0f;
                return _points[0].Y;
            }
            set
            {
                if (_points.Count == 0) return;
                var distance = value - _points[0].Y;

                _points[0] = new Vector(_points[0].X, value);


                for (var index = 0; index < _points.Count; index++)
                {
                    if (index == 0) continue;

                    _points[index] = new Vector(_points[index].X, _points[index].Y + distance);
                }

                BuildEdges();
            }
        }

        public List<Vector> Edges
        {
            get { return _edges; }
        }

        public List<Vector> Points
        {
            get { return _points; }
        }

        public Vector Center
        {
            get
            {
                float totalX = 0;
                float totalY = 0;
                for (var i = 0; i < _points.Count; i++)
                {
                    totalX += _points[i].X;
                    totalY += _points[i].Y;
                }

                return new Vector(totalX/_points.Count, totalY/_points.Count);
            }
        }

        public static Polygon FromRectangle(Rectangle rectangle)
        {
            var polygon = new Polygon();
            polygon.Points.Add(new Vector(rectangle.Left, rectangle.Top));
            polygon.Points.Add(new Vector(rectangle.Right, rectangle.Top));
            polygon.Points.Add(new Vector(rectangle.Right, rectangle.Bottom));
            polygon.Points.Add(new Vector(rectangle.Left, rectangle.Bottom));

            polygon.BuildEdges();

            return polygon;
        }

        public void BuildEdges()
        {
            _edges.Clear();
            for (var i = 0; i < _points.Count; i++)
            {
                var p1 = _points[i];
                var p2 = i + 1 >= _points.Count ? _points[0] : _points[i + 1];
                _edges.Add(p2 - p1);
            }
        }

        public Polygon Rotate(float rot)
        {
            return Rotate(rot, Center);
        }

        public Polygon Rotate(float rot, Vector origin)
        {
            Polygon polygon = new Polygon();

            for (int i = 0; i < _points.Count; i++)
            {
                Vector2 point = new Vector2(_points[i].X, _points[i].Y);
                point -= new Vector2(origin.X, origin.Y);
                point = Vector2.Transform(point, Matrix.CreateRotationY(rot));
                point += new Vector2(origin.X, origin.Y);

                polygon.Points.Add(new Vector(point));
            }

            polygon.BuildEdges();

            return polygon;
        }

        public void Offset(Vector v)
        {
            Offset(v.X, v.Y);
        }

        public void Offset(float x, float y)
        {
            for (var i = 0; i < _points.Count; i++)
            {
                var p = _points[i];
                _points[i] = new Vector(p.X + x, p.Y + y);
            }
        }

        public Polygon Clone()
        {
            Polygon clone = new Polygon();

            foreach (var point in Points)
            {
                clone.Points.Add(point);
            }

            clone.BuildEdges();
            return clone;
        }

        public override string ToString()
        {
            var result = "";

            for (var i = 0; i < _points.Count; i++)
            {
                if (result != "") result += " ";
                result += "{" + _points[i].ToString(true) + "}";
            }

            return result;
        }
    }
}