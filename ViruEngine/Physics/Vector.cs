using System;
using System.Drawing;
using Microsoft.Xna.Framework;
using Point = System.Drawing.Point;

namespace ViruEngine.Physics
{
    public struct Vector
    {
        public float X;
        public float Y;

        public Vector(float x, float y)
        {
            X = x;
            Y = y;
        }

        public Vector(Vector2 vector)
        {
            X = vector.X;
            Y = vector.Y;
        }

        public float Magnitude
        {
            get { return (float) Math.Sqrt(X*X + Y*Y); }
        }

        public static Vector FromPoint(Point p)
        {
            return FromPoint(p.X, p.Y);
        }

        public static Vector FromPoint(int x, int y)
        {
            return new Vector(x, y);
        }

        public void Normalize()
        {
            var magnitude = Magnitude;
            X = X/magnitude;
            Y = Y/magnitude;
        }

        public Vector GetNormalized()
        {
            var magnitude = Magnitude;

            return new Vector(X/magnitude, Y/magnitude);
        }

        public float DotProduct(Vector vector)
        {
            return X*vector.X + Y*vector.Y;
        }

        public float DistanceTo(Vector vector)
        {
            return (float) Math.Sqrt(Math.Pow(vector.X - X, 2) + Math.Pow(vector.Y - Y, 2));
        }

        public static implicit operator Point(Vector p)
        {
            return new Point((int) p.X, (int) p.Y);
        }

        public static implicit operator PointF(Vector p)
        {
            return new PointF(p.X, p.Y);
        }

        public static Vector operator +(Vector a, Vector b)
        {
            return new Vector(a.X + b.X, a.Y + b.Y);
        }

        public static Vector operator -(Vector a)
        {
            return new Vector(-a.X, -a.Y);
        }

        public static Vector operator -(Vector a, Vector b)
        {
            return new Vector(a.X - b.X, a.Y - b.Y);
        }

        public static Vector operator *(Vector a, float b)
        {
            return new Vector(a.X*b, a.Y*b);
        }

        public static Vector operator *(Vector a, int b)
        {
            return new Vector(a.X*b, a.Y*b);
        }

        public static Vector operator *(Vector a, double b)
        {
            return new Vector((float) (a.X*b), (float) (a.Y*b));
        }

        public override bool Equals(object obj)
        {
            var v = (Vector) obj;

            return X == v.X && Y == v.Y;
        }

        public bool Equals(Vector v)
        {
            return X == v.X && Y == v.Y;
        }

        public override int GetHashCode()
        {
            return X.GetHashCode() ^ Y.GetHashCode();
        }

        public static bool operator ==(Vector a, Vector b)
        {
            return a.X == b.X && a.Y == b.Y;
        }

        public static bool operator !=(Vector a, Vector b)
        {
            return a.X != b.X || a.Y != b.Y;
        }

        public override string ToString()
        {
            return X + ", " + Y;
        }

        public string ToString(bool rounded)
        {
            if (rounded)
            {
                return (int) Math.Round(X) + ", " + (int) Math.Round(Y);
            }
            return ToString();
        }
    }
}