﻿using System;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Xml;

namespace ViruEngine.PersistentData
{
    public struct Resolution
    {
        public int Width;
        public int Height;
    }

    public static class Settings
    {
        public static float Volume;
        public static bool Fullscreen;
        public static Resolution Resolution;

        public static void Load()
        {
            var doc = new XmlDocument();

            if (File.Exists(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "/Saves/settings.xml"))
            {
                doc.Load(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "/Saves/settings.xml");
            }
            else
                doc.Load(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "/Saves/defaultSettings.xml");

            XmlNode root = doc.DocumentElement;

            if (root != null)
                foreach (XmlNode node in root.ChildNodes)
                {
                    if (node.Name == "volume")
                    {
                        var element = (XmlElement) node;
                        Volume = float.Parse(element.InnerText);
                        continue;
                    }

                    if (node.Name == "fullscreen")
                    {
                        var element = (XmlElement) node;

                        Fullscreen = bool.Parse(element.InnerText);
                    }

                    if (node.Name == "resolution")
                    {
                        foreach (var subNode in node.ChildNodes)
                        {
                            var subElement = (XmlElement)subNode;

                            if (subElement.Name == "x") Resolution.Width = Int32.Parse(subElement.InnerText);
                            if (subElement.Name == "y") Resolution.Height = Int32.Parse(subElement.InnerText);
                        }
                    }
                }
        }

        public static void Save()
        {
            var doc = new XmlDocument();
            var declaration = doc.CreateXmlDeclaration("1.0", "utf-8", null);
            doc.AppendChild(declaration);

            var root = doc.CreateElement("saveData");

            var volume = doc.CreateElement("volume");
            volume.InnerText = Volume.ToString(CultureInfo.CurrentCulture);

            var fullscreen = doc.CreateElement("fullscreen");
            fullscreen.InnerText = Fullscreen.ToString();

            var resolution = doc.CreateElement("resolution");
                var xRes = doc.CreateElement("x");
                xRes.InnerText = Resolution.Width.ToString();

                var yRes = doc.CreateElement("y");
                yRes.InnerText = Resolution.Height.ToString();

                resolution.AppendChild(xRes);
                resolution.AppendChild(yRes);

            root.AppendChild(volume);
            root.AppendChild(fullscreen);
            root.AppendChild(resolution);

            doc.AppendChild(root);
            doc.Save(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "/Saves/settings.xml");
        }
    }
}