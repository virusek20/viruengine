﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using ViruEngine.Console;
using ViruEngine.GameStates;
using ViruEngine.Graphics.Textures;

namespace ViruEngine.GUI
{
    public class CheckBox : GuiBase
    {
        public static Texture2D BoxTexture;
        public static Texture2D CheckTexture;
        private Rectangle _hitBox1;
        private Vector2 _position;
        public new Color Color = Color.Gray;
        public Color CheckBoxCheckColor = Color.Green;
        public string Text = "";
        public Color TextColor = Color.White;
        public bool Toggled;

        public CheckBox(ViruGame game, GameState state, string id, bool noSub = false) : base(game, state, id, noSub)
        {
        }

        public new Vector2 Position
        {
            get { return _position; }
            set
            {
                _position.X = (int) value.X;
                _position.Y = (int) value.Y;

                _hitBox1 = new Rectangle((int) ((int) Game.BasicFont.MeasureString(Text).X + _position.X + 5),
                    (int) _position.Y, 20, 20);
            }
        }

        public override void Recaulculate()
        {
            var newPos = OnResize();
            _position.X = (int) newPos.X;
            _position.Y = (int) newPos.Y;

            _hitBox1 = new Rectangle((int) ((int) Game.BasicFont.MeasureString(Text).X + _position.X + 5),
                (int) _position.Y, 20, 20);
        }

        public override void Check(GameTime gameTime, Point point, bool click)
        {
            if (Visibility == HiddenState.Hidden | Visibility == HiddenState.Disabled) return;

            if (!_hitBox1.Contains(point)) return;

            try
            {
                OnMouseOver.Invoke();
            }
            catch
            {
                Game.WarningIcon.ToggleShow(10, HiddenState.Visible);
                GameConsole.WriteLine("Error: " + ID + ": OnMouseOver: " + Text.ToLower());
            }

            if (!click) return;

            Toggled = !Toggled;

            try
            {
                OnClick.Invoke();
            }
            catch
            {
                Game.WarningIcon.ToggleShow(10, HiddenState.Visible);
                GameConsole.WriteLine("Error: " + ID + ": OnClick: " + Text.ToLower());
            }
        }

        public override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            if (Visibility == HiddenState.Hidden) return;

            if (Visibility == HiddenState.Disabled)
            {
                spriteBatch.DrawString(Game.BasicFont, Text, new Vector2(_position.X, _position.Y + 2),
                    ConsoleColor);
                spriteBatch.Draw(BoxTexture,
                    new Vector2(_position.X + Game.BasicFont.MeasureString(Text).X + 5, _position.Y),
                    ConsoleColor);
                if (Toggled)
                    spriteBatch.Draw(CheckTexture,
                        new Vector2(_position.X + Game.BasicFont.MeasureString(Text).X + 5,
                            _position.Y), ConsoleColor);
            }
            else
            {
                spriteBatch.DrawString(Game.BasicFont, Text, new Vector2(_position.X, _position.Y + 2), TextColor);
                spriteBatch.Draw(BoxTexture,
                    new Vector2(_position.X + Game.BasicFont.MeasureString(Text).X + 5, _position.Y),
                    Color);
                if (Toggled)
                    spriteBatch.Draw(CheckTexture,
                        new Vector2(_position.X + Game.BasicFont.MeasureString(Text).X + 5,
                            _position.Y), CheckBoxCheckColor);
            }
        }

        [EngineLoad(false, LoadTime.LoadContent)]
        public static void Initialize()
        {
            BoxTexture = ImageTexture.FromImage("GUI/checkBox.png");
            CheckTexture = ImageTexture.FromImage("GUI/checkBoxFiller.png");
        }
    }
}