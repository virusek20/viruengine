﻿using Microsoft.Xna.Framework;

namespace ViruEngine.GUI
{
    public class MousePopup
    {
        internal static ViruGame Game;

        [EngineLoad(true, LoadTime.Init)]
        public static void Initialize(ViruGame game)
        {
            Game = game;
        }

        public static void Draw(string text, Color color, Vector2 offset)
        {
            Game.SpriteBatch.Draw(Slider.SliderTexture,
                new Rectangle((int) (Game.MState.X + offset.X), (int) (Game.MState.Y + offset.Y),
                    (int) Game.BasicFont.MeasureString(text).X,
                    (int) Game.BasicFont.MeasureString(text).Y),
                Color.LightGray);
            Game.SpriteBatch.DrawString(Game.BasicFont, text,
                new Vector2(Game.MState.X + offset.X, Game.MState.Y + offset.Y), color);
        }
    }
}