﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using ViruEngine.Console;
using ViruEngine.GameStates;

namespace ViruEngine.GUI
{
    public class Label : GuiBase
    {
        private Rectangle _label;
        private string _text;

        public Label(ViruGame game, GameState state, string id, bool noSub = false) : base(game, state, id, noSub)
        {
        }

        public new Vector2 Position
        {
            set
            {
                _label.X = (int) value.X;
                _label.Y = (int) value.Y;
            }
            get { return new Vector2(_label.X, _label.Y); }
        }

        public string Text
        {
            get { return _text; }
            set
            {
                _label.Width = (int) Game.BasicFont.MeasureString(value).X;
                _text = value;
            }
        }

        public override void Recaulculate()
        {
            Position = OnResize();
        }

        public override void Check(GameTime gameTime, Point point, bool click)
        {
            if (Visibility == HiddenState.Hidden | Visibility == HiddenState.Disabled) return;

            if (!_label.Contains(point)) return;

            try
            {
                OnMouseOver.Invoke();
            }
            catch
            {
                Game.WarningIcon.ToggleShow(10, HiddenState.Visible);
                GameConsole.WriteLine("Error: " + ID + ": OnMouseOver: " + Text.ToLower());
            }

            if (!click) return;

            try
            {
                OnClick.Invoke();
            }
            catch
            {
                Game.WarningIcon.ToggleShow(10, HiddenState.Visible);
                GameConsole.WriteLine("Error: " + ID + ": OnClick: " + Text.ToLower());
            }
        }

        public override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            if (Visibility == HiddenState.Hidden) return;

            spriteBatch.DrawString(Game.BasicFont, _text, Position,
                Visibility == HiddenState.Disabled ? ConsoleColor : Color);
        }
    }
}