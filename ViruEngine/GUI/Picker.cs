﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using ViruEngine.Console;
using ViruEngine.GameStates;
using ViruEngine.Graphics.Textures;

namespace ViruEngine.GUI
{
    public class Picker : GuiBase
    {
        private static Texture2D _pickerTexture;
        private Rectangle _hitPos1;
        private Rectangle _hitPos2;
        private Rectangle _picker = new Rectangle(0, 0, 200, 20);
        public int Selection;
        public string[] Text;
        public Color TextColor = Color.White;

        public Picker(ViruGame game, GameState state, string id, bool noSub = false) : base(game, state, id, noSub)
        {
        }

        public Vector2 Positon
        {
            get { return new Vector2(_picker.X, _picker.Y); }
            set
            {
                _picker.X = (int) value.X;
                _picker.Y = (int) value.Y;

                _hitPos1 = new Rectangle(_picker.X, _picker.Y, 17, 20);
                _hitPos2 = new Rectangle(_picker.X + 181, _picker.Y, 17, 20);
            }
        }

        public override void Recaulculate()
        {
            var newPos = OnResize();

            _picker.X = (int) newPos.X;
            _picker.Y = (int) newPos.Y;

            _hitPos1 = new Rectangle(_picker.X, _picker.Y, 17, 20);
            _hitPos2 = new Rectangle(_picker.X + 181, _picker.Y, 17, 20);
        }

        public override void Check(GameTime gameTime, Point point, bool click)
        {
            if (Visibility == HiddenState.Hidden | Visibility == HiddenState.Disabled) return;

            if (_picker.Contains(point))
            {
                try
                {
                    OnMouseOver.Invoke();
                }
                catch
                {
                    Game.WarningIcon.ToggleShow(10, HiddenState.Visible);
                    GameConsole.WriteLine("Error: Picker: OnMouseOver: " + Text[Selection].ToLower());
                }

                if (!click) return;

                try
                {
                    OnClick.Invoke();
                }
                catch
                {
                    Game.WarningIcon.ToggleShow(10, HiddenState.Visible);
                    GameConsole.WriteLine("Error: Picker: OnClick: " + Text[Selection].ToLower());
                }
            }

            if (_hitPos1.Contains(point) & click)
            {
                Selection--;
                if (Selection < 0)
                {
                    Selection = Text.Length - 1;
                }
            }
            else if (_hitPos2.Contains(point) & click)
            {
                Selection++;
                if (Selection > Text.Length - 1)
                {
                    Selection = 0;
                }
            }
        }

        public override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            if (Visibility == HiddenState.Hidden) return;

            spriteBatch.Draw(_pickerTexture, _picker, Color.White);

            spriteBatch.DrawString(Game.BasicFont, Text[Selection],
                new Vector2(
                    _picker.X + 100 - Game.BasicFont.MeasureString(Text[Selection]).X/2,
                    _picker.Y), Visibility == HiddenState.Disabled ? ConsoleColor : TextColor);
        }

        [EngineLoad(false, LoadTime.LoadContent)]
        public static void Initialize()
        {
            _pickerTexture = ImageTexture.FromImage("GUI/listPicker.png");
        }

        public string GetSelected()
        {
            return Text[Selection];
        }
    }
}