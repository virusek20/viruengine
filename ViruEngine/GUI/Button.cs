﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using ViruEngine.Console;
using ViruEngine.GameStates;

namespace ViruEngine.GUI
{
    public class Button : GuiBase
    {
        private Rectangle _button;
        private Color _color;
        private string _text = "";
        public Color OverColor = Color.Red;

        public Button(ViruGame game, GameState state, string id, bool noSub = false) : base(game, state, id, noSub)
        {
        }

        public new Vector2 Position
        {
            set
            {
                _button.X = (int) value.X;
                _button.Y = (int) value.Y;
            }
            get { return new Vector2(_button.X, _button.Y); }
        }

        public string Text
        {
            get { return _text; }
            set
            {
                _text = value;
                _button = new Rectangle((int) Position.X, (int) Position.Y, (int) Game.BasicFont.MeasureString(value).X,
                    (int) Game.BasicFont.MeasureString(value).Y);
            }
        }

        public override void Recaulculate()
        {
            Position = OnResize();
            _button = new Rectangle((int) Position.X, (int) Position.Y, (int) Game.BasicFont.MeasureString(_text).X,
                (int) Game.BasicFont.MeasureString(_text).Y);
        }

        public override void Check(GameTime gameTime, Point point, bool click)
        {
            if (Visibility == HiddenState.Hidden | Visibility == HiddenState.Disabled) return;

            CustomUpdate(gameTime, point, click);

            _color = Color;

            if (!_button.Contains(point))
            {
                return;
            }

            _color = OverColor;

            if (!click) return;
            try
            {
                OnClick.Invoke();
            }
            catch (Exception e)
            {
                Game.WarningIcon.ToggleShow(10, HiddenState.Visible);
                GameConsole.WriteLine("Error: " + ID + ": OnClick: " + Text.ToLower());
                System.Console.WriteLine(e);
            }
        }

        public override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            if (Visibility == HiddenState.Hidden) return;

            CustomDraw(spriteBatch);

            if (_color == OverColor & Visibility == HiddenState.Visible)
            {
                try
                {
                    OnMouseOver.Invoke();
                }
                catch
                {
                    Game.WarningIcon.ToggleShow(10, HiddenState.Visible);
                    GameConsole.WriteLine("Error: " + ID + ": OnMouseOver: " + Text.ToLower());
                }
            }

            if (Visibility == HiddenState.Disabled) _color = ConsoleColor;

            spriteBatch.DrawString(Game.BasicFont, _text, new Vector2(_button.X, _button.Y), _color);
        }
    }
}