﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using ViruEngine.Console;
using ViruEngine.GameStates;

namespace ViruEngine.GUI
{
    public class TexturedButton : GuiBase
    {
        private Rectangle _button;
        private bool _color;
        private float _scale = 1f;
        private int _textureSize;
        public Color OverColor = Color.Red;
        public Texture2D OverTexture2D;
        public Texture2D Texture;

        public TexturedButton(ViruGame game, GameState state, string id, bool noSub = false)
            : base(game, state, id, noSub)
        {
        }

        public new Vector2 Position
        {
            set
            {
                _button.X = (int) value.X;
                _button.Y = (int) value.Y;
            }
            get { return new Vector2(_button.X, _button.Y); }
        }

        public int TextureSize
        {
            get { return _textureSize; }
            set
            {
                _textureSize = value;
                _button.Width = (int) (value*Scale);
                _button.Height = (int) (value*Scale);
            }
        }

        public float Scale
        {
            get { return _scale; }
            set
            {
                _scale = value;
                _button.Width = (int) (TextureSize*value);
                _button.Height = (int) (TextureSize*value);
            }
        }

        public override void Recaulculate()
        {
            var newPos = OnResize();

            _button.X = (int) newPos.X;
            _button.Y = (int) newPos.Y;
        }

        public override void Check(GameTime gameTime, Point point, bool click)
        {
            if (Visibility == HiddenState.Hidden | Visibility == HiddenState.Disabled) return;

            _color = false;

            if (_button.Contains(point))
            {
                _color = true;

                try
                {
                    OnMouseOver.Invoke();
                }
                catch
                {
                    Game.WarningIcon.ToggleShow(10, HiddenState.Visible);
                    GameConsole.WriteLine("Error: TexturedButton: OnMouseClick: unknown");
                }

                if (click)
                {
                    try
                    {
                        OnClick.Invoke();
                    }
                    catch
                    {
                        Game.WarningIcon.ToggleShow(10, HiddenState.Visible);
                        GameConsole.WriteLine("Error: TexturedButton: OnMouseClick: unknown");
                    }
                }
            }
        }

        public override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            if (Visibility == HiddenState.Hidden) return;

            if (Visibility == HiddenState.Disabled)
            {
                spriteBatch.Draw(Texture, _button, new Rectangle(0, 0, TextureSize, TextureSize), ConsoleColor);
                return;
            }


            if (_color)
            {
                spriteBatch.Draw(OverTexture2D ?? Texture, _button, new Rectangle(0, 0, TextureSize, TextureSize),
                    OverColor);
            }
            else
            {
                spriteBatch.Draw(Texture, _button, new Rectangle(0, 0, TextureSize, TextureSize), Color);
            }
        }
    }
}