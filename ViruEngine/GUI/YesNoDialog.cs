﻿using System.Timers;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using ViruEngine.GameStates;

namespace ViruEngine.GUI
{
    public class YesNoDialog : GuiBase
    {
        private readonly Button _buttonNo;
        private readonly Button _buttonYes;
        private readonly GameState _state;
        private Vector2 _position;
        public string Text = "";

        public YesNoDialog(ViruGame game, GameState state, string id, bool noSub = false) : base(game, state, id, noSub)
        {
            _state = state;

            _buttonYes = new Button(Game, state, id + "ButtonYes", true)
            {
                Position = new Vector2(20 + Position.X, 165 + Position.Y),
                Text = "Yes",
                OnResize = () => new Vector2(20 + Position.X, 165 + Position.Y)
            };

            _buttonNo = new Button(Game, state, id + "ButtonNo", true)
            {
                Position = new Vector2(580 - Game.BasicFont.MeasureString("No").X + Position.X, 165 + Position.Y),
                Text = "No",
                OnResize =
                    () =>
                        new Vector2(580 - Game.BasicFont.MeasureString(_buttonNo.Text).X + Position.X, 165 + Position.Y)
            };

            Visibility = HiddenState.Hidden;
        }

        public new Vector2 Position
        {
            get { return _position; }
            set
            {
                _position = value;
                _buttonNo.Position = new Vector2(580 - Game.BasicFont.MeasureString(_buttonNo.Text).X + Position.X,
                    165 + Position.Y);
                _buttonYes.Position = new Vector2(20 + Position.X, 165 + Position.Y);
            }
        }

        public new int Priority
        {
            get { return _buttonNo.Priority; }
            set
            {
                _buttonNo.Priority = value;
                _buttonYes.Priority = value;
            }
        }

        public string YesText
        {
            set { _buttonYes.Text = value; }
            get { return _buttonYes.Text; }
        }

        public string NoText
        {
            set
            {
                _buttonNo.Text = value;
                _buttonNo.Position = new Vector2(580 - Game.BasicFont.MeasureString(_buttonNo.Text).X + Position.X,
                    165 + Position.Y);
            }
            get { return _buttonNo.Text; }
        }

        public Color OverColor
        {
            set
            {
                _buttonYes.OverColor = value;
                _buttonNo.OverColor = value;
            }
        }

        public Color TextColor
        {
            set
            {
                _buttonYes.Color = value;
                _buttonNo.Color = value;
            }
        }

        public ButtonDelegate OnYes
        {
            set { _buttonYes.OnClick = value; }
        }

        public ButtonDelegate OnNo
        {
            set { _buttonNo.OnClick = value; }
        }

        public new void Recaulculate()
        {
            var newPos = OnResize();

            Position = newPos;

            _buttonNo.Recaulculate();
            _buttonYes.Recaulculate();
        }

        public override void Check(GameTime gameTime, Point point, bool click)
        {
            if (Visibility == HiddenState.Hidden) return;

            _buttonYes.Check(gameTime, point, click);
            _buttonNo.Check(gameTime, point, click);
        }

        public override void ToggleShow(int priority, HiddenState setState)
        {
            base.ToggleShow(priority, setState);

            if (!(Priority < priority | priority == -1)) Visibility = setState;

            _buttonNo.Visibility = setState;
            _buttonYes.Visibility = setState;

            if (setState != HiddenState.Visible) return;

            var timer = new Timer(0.01);
            timer.Elapsed += (sender, args) =>
            {
                _state.DoHide(Priority - 1, HiddenState.Disabled);
                timer.Stop();
            };
            timer.Start();
        }

        public override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            if (Visibility == HiddenState.Hidden) return;

            spriteBatch.Draw(Slider.SliderTexture, new Rectangle((int) Position.X, (int) Position.Y, 600, 200),
                null, Visibility == HiddenState.Disabled ? ConsoleColor : Color);
            spriteBatch.DrawString(Game.BasicFont, Text, new Vector2(15, 13) + Position,
                Visibility == HiddenState.Disabled ? ConsoleColor : _buttonNo.Color);

            _buttonNo.Draw(gameTime, spriteBatch);
            _buttonYes.Draw(gameTime, spriteBatch);
        }

        public override void Enable(int priority)
        {
            if (Visibility == HiddenState.Hidden | DisabledTimes != 1 | (priority < Priority & priority != -1)) return;
            base.Enable(priority);

            _buttonNo.Enable(priority);
            _buttonYes.Enable(priority);
        }

        public override void Disable(int priority)
        {
            if (Visibility == HiddenState.Hidden | (priority < Priority & priority != -1)) return;
            if (Visibility == HiddenState.Disabled & LastVisibility != HiddenState.Visible)
                LastVisibility = HiddenState.Disabled;
            if (Visibility == HiddenState.Disabled) return;
            base.Disable(priority);

            _buttonNo.Disable(priority);
            _buttonYes.Disable(priority);
        }

        public void Show()
        {
            if (Visibility == HiddenState.Visible) return;
            Visibility = HiddenState.Visible;
            _state.DoDisable(Priority - 1);
        }

        public void Hide()
        {
            if (Visibility == HiddenState.Hidden) return;
            Visibility = HiddenState.Hidden;
            _state.DoEnable(Priority - 1);
        }
    }
}