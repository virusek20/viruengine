﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using ViruEngine.Console;
using ViruEngine.GameStates;
using ViruEngine.Graphics.Textures;

namespace ViruEngine.GUI
{
    public class Slider : GuiBase
    {
        public static Texture2D SliderTexture;
        private Rectangle _hitBox;
        private Rectangle _slider;
        public bool DrawText = false;
        public float Progress;
        public Color SliderColor = Color.LightGray;
        public Color SliderColorActive = Color.Lime;
        public string TextAppend = "";
        public Color TextColor = Color.White;

        public Slider(ViruGame game, GameState state, string id, bool noSub = false) : base(game, state, id, noSub)
        {
        }

        public new Vector2 Position
        {
            get { return new Vector2(_slider.X, _slider.Y); }
            set
            {
                _slider.X = (int) value.X;
                _slider.Y = (int) value.Y;

                _slider = new Rectangle((int) value.X, (int) value.Y, _slider.Width, 10);
                _hitBox = new Rectangle((int) value.X, (int) value.Y, _slider.Width + 10, 10);
            }
        }

        public int Width
        {
            get { return _slider.Width; }
            set
            {
                _slider.Width = value;
                _hitBox.Width = _slider.Width + 10;
            }
        }

        public override void Recaulculate()
        {
            var position = OnResize();

            _slider.X = (int) position.X;
            _slider.Y = (int) position.Y;

            _slider = new Rectangle((int) position.X, (int) position.Y, _slider.Width, 10);
            _hitBox = new Rectangle((int) position.X, (int) position.Y, _slider.Width + 10, 10);
        }

        public override void Check(GameTime gameTime, Point point, bool click)
        {
            if (Visibility == HiddenState.Hidden | Visibility == HiddenState.Disabled) return;

            if (!_hitBox.Contains(point)) return;

            try
            {
                OnMouseOver.Invoke();
            }
            catch
            {
                Game.WarningIcon.ToggleShow(10, HiddenState.Visible);
                GameConsole.WriteLine("Error: Slider: OnMouseOver: unknown");
            }

            if (Game.MState.LeftButton != ButtonState.Pressed) return;

            try
            {
                OnClick.Invoke();
            }
            catch
            {
                Game.WarningIcon.ToggleShow(10, HiddenState.Visible);
                GameConsole.WriteLine("Error: Slider: OnMouseClick: unknown");
            }

            Progress = (point.X - _slider.X)/(float) _slider.Width;
            if (Progress > 1) Progress = 1;
        }

        public override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            if (Visibility == HiddenState.Hidden) return;
            spriteBatch.Draw(SliderTexture, new Vector2(_slider.X, _slider.Y), null, SliderColor, 0,
                new Vector2(0, 0), new Vector2(_slider.Width, 10), SpriteEffects.None, 0);
            spriteBatch.Draw(SliderTexture, new Vector2(_slider.X, _slider.Y), null,
                Visibility == HiddenState.Disabled ? ConsoleColor : SliderColorActive, 0,
                new Vector2(0, 0), new Vector2(_slider.Width*Progress, 10), SpriteEffects.None, 0);

            if (DrawText)
                spriteBatch.DrawString(Game.BasicFont, Math.Floor(Progress*100) + TextAppend,
                    new Vector2(
                        _slider.X + (_slider.Width/2) -
                        (Game.BasicFont.MeasureString(Math.Floor(Progress*100) + TextAppend).X -
                         15)/2, _slider.Y - 1),
                    Visibility == HiddenState.Disabled ? ConsoleColor : TextColor, 0,
                    new Vector2(0, 0), 0.6f,
                    SpriteEffects.None, 0);
        }

        [EngineLoad(false, LoadTime.LoadContent)]
        public static void Initialize()
        {
            SliderTexture = SinglePixelTexture.GetSinglePixelTexture(new Color(100, 100, 100, 230));
        }
    }
}