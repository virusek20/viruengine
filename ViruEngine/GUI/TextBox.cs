﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using ViruEngine.GameStates;
using ViruEngine.Graphics.Rendering;
using ViruEngine.Graphics.Textures;

namespace ViruEngine.GUI
{
    public class TextBox : GuiBase
    {
        public static SpriteFont SmallFont;
        private static Texture2D _pixTexture;
        private RenderablePolygon _buttonDown = new RenderablePolygon(new Rectangle(0, 0, 32, 32), 0f, true);
        private RenderablePolygon _buttonDownInner = new RenderablePolygon(new Rectangle(0, 0, 32, 32), 0f, true);
        private RenderablePolygon _buttonUp = new RenderablePolygon(new Rectangle(0, 0, 32, 32), 0f, true);
        private RenderablePolygon _buttonUpInner = new RenderablePolygon(new Rectangle(0, 0, 32, 32), 0f, true);
        private Rectangle _dimensions;
        private int _lastLine;
        public Color BackgroundColor;
        public Color BorderColor;
        public List<Tuple<string, Color>> Lines = new List<Tuple<string, Color>>();

        public TextBox(ViruGame game, GameState state, string id, bool noSub = false) : base(game, state, id, noSub)
        {
        }

        public Rectangle Dimensions
        {
            set
            {
                _dimensions = value;

                _buttonUp = new RenderablePolygon(new[]
                {
                    new Vector2(_dimensions.X + _dimensions.Width + 20, _dimensions.Y + _dimensions.Height/2),
                    new Vector2(_dimensions.X + _dimensions.Width, _dimensions.Y + _dimensions.Height/2),
                    new Vector2(_dimensions.X + _dimensions.Width + 20, (_dimensions.Y + _dimensions.Height/2) - 44),
                    new Vector2(_dimensions.X + _dimensions.Width, (_dimensions.Y + _dimensions.Height/2) - 64)
                }, true);

                _buttonDown = new RenderablePolygon(new[]
                {
                    new Vector2(_dimensions.X + _dimensions.Width, (_dimensions.Y + _dimensions.Height/2) + 64),
                    new Vector2(_dimensions.X + _dimensions.Width, (_dimensions.Y + _dimensions.Height/2)),
                    new Vector2(_dimensions.X + _dimensions.Width + 20, (_dimensions.Y + _dimensions.Height/2) + 44),
                    new Vector2(_dimensions.X + _dimensions.Width + 20, (_dimensions.Y + _dimensions.Height/2))
                }, true);

                _buttonUpInner = new RenderablePolygon(new[]
                {
                    new Vector2(_dimensions.X + _dimensions.Width + 19, _dimensions.Y + _dimensions.Height/2),
                    new Vector2(_dimensions.X + _dimensions.Width, _dimensions.Y + _dimensions.Height/2),
                    new Vector2(_dimensions.X + _dimensions.Width + 19, (_dimensions.Y + _dimensions.Height/2) - 43),
                    new Vector2(_dimensions.X + _dimensions.Width, (_dimensions.Y + _dimensions.Height/2) - 62)
                }, true);

                _buttonDownInner = new RenderablePolygon(new[]
                {
                    new Vector2(_dimensions.X + _dimensions.Width, (_dimensions.Y + _dimensions.Height/2) + 62),
                    new Vector2(_dimensions.X + _dimensions.Width, (_dimensions.Y + _dimensions.Height/2) + 1),
                    new Vector2(_dimensions.X + _dimensions.Width + 19, (_dimensions.Y + _dimensions.Height/2) + 43),
                    new Vector2(_dimensions.X + _dimensions.Width + 19, (_dimensions.Y + _dimensions.Height/2) + 1)
                }, true);

                _buttonDown.Color = Color.Gray;
                _buttonUp.Color = Color.Gray;
                _buttonDownInner.Color = Color.Black;
                _buttonUpInner.Color = Color.Black;
            }
            get { return _dimensions; }
        }

        public new Vector2 Position
        {
            set
            {
                _dimensions.X = (int) value.X;
                _dimensions.Y = (int) value.Y;

                _buttonUp = new RenderablePolygon(new[]
                {
                    new Vector2(_dimensions.X + _dimensions.Width + 20, _dimensions.Y + _dimensions.Height/2),
                    new Vector2(_dimensions.X + _dimensions.Width, _dimensions.Y + _dimensions.Height/2),
                    new Vector2(_dimensions.X + _dimensions.Width + 20, (_dimensions.Y + _dimensions.Height/2) - 44),
                    new Vector2(_dimensions.X + _dimensions.Width, (_dimensions.Y + _dimensions.Height/2) - 64)
                }, true);

                _buttonDown = new RenderablePolygon(new[]
                {
                    new Vector2(_dimensions.X + _dimensions.Width, (_dimensions.Y + _dimensions.Height/2) + 64),
                    new Vector2(_dimensions.X + _dimensions.Width, (_dimensions.Y + _dimensions.Height/2)),
                    new Vector2(_dimensions.X + _dimensions.Width + 20, (_dimensions.Y + _dimensions.Height/2) + 44),
                    new Vector2(_dimensions.X + _dimensions.Width + 20, (_dimensions.Y + _dimensions.Height/2))
                }, true);

                _buttonDown.Color = Color.Gray;
                _buttonUp.Color = Color.Gray;
            }
            get { return new Vector2(_dimensions.X, _dimensions.Y); }
        }

        public Vector2 Size
        {
            set
            {
                _dimensions.Width = (int) value.X;
                _dimensions.Height = (int) value.Y;

                _buttonUp = new RenderablePolygon(new[]
                {
                    new Vector2(_dimensions.X + _dimensions.Width + 20, _dimensions.Y + _dimensions.Height/2),
                    new Vector2(_dimensions.X + _dimensions.Width, _dimensions.Y + _dimensions.Height/2),
                    new Vector2(_dimensions.X + _dimensions.Width + 20, (_dimensions.Y + _dimensions.Height/2) - 44),
                    new Vector2(_dimensions.X + _dimensions.Width, (_dimensions.Y + _dimensions.Height/2) - 64)
                }, true);

                _buttonDown = new RenderablePolygon(new[]
                {
                    new Vector2(_dimensions.X + _dimensions.Width, (_dimensions.Y + _dimensions.Height/2) + 64),
                    new Vector2(_dimensions.X + _dimensions.Width, (_dimensions.Y + _dimensions.Height/2)),
                    new Vector2(_dimensions.X + _dimensions.Width + 20, (_dimensions.Y + _dimensions.Height/2) + 44),
                    new Vector2(_dimensions.X + _dimensions.Width + 20, (_dimensions.Y + _dimensions.Height/2))
                }, true);

                _buttonUpInner = new RenderablePolygon(new[]
                {
                    new Vector2(_dimensions.X + _dimensions.Width + 20, _dimensions.Y + _dimensions.Height/2),
                    new Vector2(_dimensions.X + _dimensions.Width, _dimensions.Y + _dimensions.Height/2),
                    new Vector2(_dimensions.X + _dimensions.Width + 20, (_dimensions.Y + _dimensions.Height/2) - 44),
                    new Vector2(_dimensions.X + _dimensions.Width, (_dimensions.Y + _dimensions.Height/2) - 64)
                }, true);

                _buttonDownInner = new RenderablePolygon(new[]
                {
                    new Vector2(_dimensions.X + _dimensions.Width, (_dimensions.Y + _dimensions.Height/2) + 64),
                    new Vector2(_dimensions.X + _dimensions.Width, (_dimensions.Y + _dimensions.Height/2)),
                    new Vector2(_dimensions.X + _dimensions.Width + 20, (_dimensions.Y + _dimensions.Height/2) + 44),
                    new Vector2(_dimensions.X + _dimensions.Width + 20, (_dimensions.Y + _dimensions.Height/2))
                }, true);

                _buttonDown.Color = Color.Gray;
                _buttonUp.Color = Color.Gray;
                _buttonDownInner.Color = Color.Black;
                _buttonUpInner.Color = Color.Black;
            }
            get { return new Vector2(_dimensions.Width, _dimensions.Height); }
        }

        [EngineLoad(true, LoadTime.LoadContent)]
        public static void Initialize(ViruGame game)
        {
            SmallFont = game.Content.Load<SpriteFont>("Fonts/Arial_small");
            _pixTexture = SinglePixelTexture.GetSinglePixelTexture(Color.White);
        }

        public override void Recaulculate()
        {
            Position = OnResize();
        }

        public void WriteLine(string line)
        {
            WriteLine(line, Color.White);
        }

        public void WriteLine(string line, Color color)
        {
            var lines = line.Split('\n');

            if (lines.Length > 1)
            {
                for (var i = 0; i < lines.Length; i++)
                {
                    WriteLine(lines[i], color);
                }
                return;
            }

            Lines.Insert(0, new Tuple<string, Color>(line, color));
        }

        public override void Check(GameTime gameTime, Point point, bool click)
        {
            if (Visibility == HiddenState.Hidden | Visibility == HiddenState.Disabled) return;

            if (_buttonUpInner.Intersects(new RenderablePolygon(new Rectangle(point.X, point.Y, 1, 1), 0f)) & click)
            {
                _lastLine++;

                if (_lastLine > Lines.Count - 1) _lastLine = Lines.Count - 1;
            }

            else if (
                _buttonDownInner.Intersects(new RenderablePolygon(new Rectangle(point.X, point.Y, 1, 1), 0f, true)) &
                click)
            {
                _lastLine--;
                if (_lastLine < 0) _lastLine = 0;
            }
        }

        public override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            if (Visibility == HiddenState.Hidden) return;

            spriteBatch.End();

            _buttonDown.Draw(Game.GraphicsDevice);
            _buttonUp.Draw(Game.GraphicsDevice);

            _buttonDownInner.Draw(Game.GraphicsDevice);
            _buttonUpInner.Draw(Game.GraphicsDevice);


            spriteBatch.Begin();

            spriteBatch.Draw(_pixTexture, _dimensions, BorderColor);
            spriteBatch.Draw(_pixTexture,
                new Rectangle(_dimensions.X + 1, _dimensions.Y + 3, _dimensions.Width - 2, _dimensions.Height - 6),
                BackgroundColor);

            spriteBatch.Draw(_pixTexture, new Rectangle(_dimensions.X + 1, _dimensions.Y + 1, _dimensions.Width - 2, 1),
                BackgroundColor);
            spriteBatch.Draw(_pixTexture,
                new Rectangle(_dimensions.X + 1, _dimensions.Y + _dimensions.Height - 2, _dimensions.Width - 2, 1),
                BackgroundColor);

            for (var i = 0; i < (_dimensions.Height/SmallFont.MeasureString("A").Y) - 2; i++)
            {
                if (i + _lastLine == Lines.Count) break;
                spriteBatch.DrawString(SmallFont, Lines[i + _lastLine].Item1,
                    new Vector2(_dimensions.X + 6,
                        _dimensions.Height - 5 + _dimensions.Y - (SmallFont.MeasureString("A").Y*(i + 1))),
                    Lines[i + _lastLine].Item2);
            }
        }
    }
}