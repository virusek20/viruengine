﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using ViruEngine.GameStates;
using ViruEngine.Graphics.Rendering;
using ViruEngine.Graphics.Textures;

namespace ViruEngine.GUI
{
    public class Canvas : GuiBase
    {
        private static Texture2D _backgroundTexture;
        public readonly Camera2D CanvasCamera;
        private RenderTarget2D _renderTarget;
        private Vector2 _size;
        public Color BackgroundColor = Color.Black;
        public Color BorderColor = Color.Gray;
        public new Vector2 Position;

        public Canvas(ViruGame game, GameState state, string id, bool noSub = false) : base(game, state, id, noSub)
        {
            CanvasCamera = new Camera2D(game)
            {
                CanvasMode = true
            };
        }

        public Vector2 CameraPosition
        {
            get { return CanvasCamera.Pos; }
            set { CanvasCamera.Pos = value; }
        }

        public float Zoom
        {
            get { return CanvasCamera.Zoom; }
            set { CanvasCamera.Zoom = value; }
        }

        public float Rotation
        {
            get { return CanvasCamera.Rotation; }
            set { CanvasCamera.Rotation = value; }
        }

        public Vector2 Size
        {
            get { return _size; }
            set
            {
                _size = value;
                _renderTarget = new RenderTarget2D(Game.GraphicsDevice, (int) value.X - 1, (int) value.Y - 1);
            }
        }

        [EngineLoad(false, LoadTime.LoadContent)]
        public static void Initialize()
        {
            _backgroundTexture = SinglePixelTexture.GetSinglePixelTexture(new Color(255, 255, 255, 255));
        }

        public override void Recaulculate()
        {
            Position = OnResize();
        }

        public override void Check(GameTime gameTime, Point point, bool click)
        {
            CustomUpdate(gameTime, point, click);
        }

        public override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            spriteBatch.End();

            Game.GraphicsDevice.SetRenderTarget(_renderTarget);
            Game.GraphicsDevice.Clear(Color.Transparent);

            spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.NonPremultiplied, SamplerState.PointWrap,
                DepthStencilState.Default, RasterizerState.CullNone, null,
                CanvasCamera.GetTransformation(Game.GraphicsDevice));

            CustomDraw(spriteBatch);

            spriteBatch.End();

            Game.GraphicsDevice.SetRenderTarget(Game.MainRenderTarget);

            spriteBatch.Begin();

            spriteBatch.Draw(_backgroundTexture,
                new Rectangle((int) Position.X - 1, (int) Position.Y - 1, (int) Size.X + 1, (int) Size.Y + 1),
                BorderColor);
            spriteBatch.Draw(_backgroundTexture,
                new Rectangle((int) Position.X, (int) Position.Y, (int) Size.X - 1, (int) Size.Y - 1), BackgroundColor);
            spriteBatch.Draw(_renderTarget, Position, Color.White);
        }
    }
}