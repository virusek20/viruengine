﻿using System;
using System.Timers;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using ViruEngine.Console;
using ViruEngine.GameStates;

namespace ViruEngine.GUI
{
    public delegate void ButtonDelegate();

    public delegate void CustomDrawDelegate(SpriteBatch spriteBatch);

    public delegate void CustomUpdateDelegate(GameTime gameTime, Point clickPoint, bool click);

    public delegate Vector2 PositionRecalculationDelegate();

    public enum HiddenState
    {
        Visible,
        Disabled,
        Hidden
    }

    public class GuiBase : IDisposable
    {
        protected readonly Timer AutoCloseTimer = new Timer();
        protected readonly ViruGame Game;
        public int AutoClose = 0;
        public Color Color = Color.White;
        public Color ConsoleColor = new Color(70, 70, 70);
        public CustomDrawDelegate CustomDraw = spriteBatch => { };
        public CustomUpdateDelegate CustomUpdate = (time, point, click) => { };
        protected int DisabledTimes;
        public string ID = "undefined";
        protected HiddenState LastVisibility = HiddenState.Hidden;
        public ButtonDelegate OnClick = () => { };
        public ButtonDelegate OnMouseOver = () => { };

        public PositionRecalculationDelegate OnResize = () =>
        {
            GameConsole.WriteLine("Error: undefined: NoResize: unknown");
            return new Vector2(0, 0);
        };

        public Vector2 Position;
        public int Priority = 0;
        public HiddenState Visibility = HiddenState.Visible;

        public GuiBase(ViruGame game, GameState state, string id, bool noSub = false)
        {
            Game = game;
            ID = id;

            if (noSub) return;

            state.OnCheck += Check;
            state.OnDraw += Draw;
            state.OnResize += Recaulculate;
            state.OnHide += ToggleShow;
            state.OnDisable += Disable;
            state.OnEnable += Enable;
        }

        public void Dispose()
        {
            AutoCloseTimer.Dispose();
        }

        public virtual void Enable(int priority)
        {
            if (Visibility == HiddenState.Disabled) DisabledTimes--;
            if (Visibility == HiddenState.Hidden | DisabledTimes != 0 | (priority < Priority & priority != -1)) return;
            Visibility = HiddenState.Visible;
        }

        public virtual void Disable(int priority)
        {
            if (Visibility == HiddenState.Hidden | (priority < Priority & priority != -1)) return;
            DisabledTimes++;
            if (Visibility == HiddenState.Disabled & LastVisibility != HiddenState.Visible)
                LastVisibility = HiddenState.Disabled;
            if (Visibility == HiddenState.Disabled) return;
            LastVisibility = Visibility;
            Visibility = HiddenState.Disabled;
        }

        public virtual void ToggleShow(int priority, HiddenState setState)
        {
            if (Priority < priority | priority == -1) Visibility = setState;

            if (!(AutoClose > 0 & setState == HiddenState.Visible)) return;

            AutoCloseTimer.Stop();
            AutoCloseTimer.Interval = AutoClose*1000;
            AutoCloseTimer.Elapsed += (sender, args) => ToggleShow(priority, HiddenState.Hidden);
            AutoCloseTimer.Start();
        }

        public virtual void Check(GameTime gameTime, Point point, bool click)
        {
        }

        public virtual void Recaulculate()
        {
        }

        public virtual void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
        }
    }
}