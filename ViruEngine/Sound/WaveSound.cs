﻿using System.IO;
using Microsoft.Xna.Framework.Audio;

namespace ViruEngine.Sound
{
    internal static class WaveSound
    {
        internal static ViruGame Game;

        [EngineLoad(true, LoadTime.Init)]
        public static void Initialize(ViruGame game)
        {
            Game = game;
        }

        public static SoundEffect FromFile(string fileName)
        {
            using (var stream = new FileStream(Game.Content.RootDirectory + fileName, FileMode.Open))
            {
                return SoundEffect.FromStream(stream);
            }
        }
    }
}