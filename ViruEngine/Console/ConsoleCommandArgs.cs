﻿using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace ViruEngine.Console
{
    public class ConsoleCommandArgs
    {
        public List<string> Arguments;

        public static explicit operator ConsoleCommandArgs(string command)
        {
            var conCMDArgs = new ConsoleCommandArgs
            {
                Arguments = Regex
                    .Matches(command, "(?<match>[^\\s\"]+)|\"(?<match>[^\"]*)\"")
                    .Cast<Match>()
                    .Select(m => m.Groups["match"].Value)
                    .ToList()
            };

            return conCMDArgs;
        }
    }
}