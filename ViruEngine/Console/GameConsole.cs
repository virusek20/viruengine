﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using NLua;
using ViruEngine.Graphics.Textures;

namespace ViruEngine.Console
{
    public static class GameConsole
    {
        public delegate void ConsoleCommandDelegate();

        public static SpriteFont ConsoleFont;
        internal static ViruGame Game;
        public static Texture2D ConsoleBckg;
        public static bool Active;
        private static string[] _lines;
        private static Color[] _colors;
        private static double _lastActive;
        private static string _inputLine = "";
        public static List<ConsoleCommand> Commands = new List<ConsoleCommand>();
        private static readonly List<String> CommandHistory = new List<string>();
        private static int _historyList = -1;
        public static Lua Lua;

        [EngineLoad(true, LoadTime.Init)]
        public static void Initialize(ViruGame game)
        {
            Game = game;

            Lua = new Lua();

            ConsoleFont = Game.BasicFont;

            _lines = new string[Game.Graphics.PreferredBackBufferHeight/16/2];
            _colors = new Color[Game.Graphics.PreferredBackBufferHeight/16/2];

            ConsoleBckg = SinglePixelTexture.GetSinglePixelTexture(new Color(100, 100, 100, 230));

            LoadCommands(Assembly.GetExecutingAssembly().GetTypes());
            LoadCommands(Assembly.GetEntryAssembly().GetTypes());

            WriteLine("Loaded " + Commands.Count + " commands");
        }

        private static void LoadCommands(Type[] types)
        {
            foreach (var command in from type in types
                where type.IsSubclassOf(typeof (ConsoleCommand))
                select (ConsoleCommand) type.GetConstructor(new[] {typeof (ViruGame)}).Invoke(new object[] {Game}))
            {
                Commands.Add(command);
            }
        }

        public static void RecalculateSizes()
        {
            if (Game == null) return;
            _lines = new string[Game.Graphics.PreferredBackBufferHeight/16/2];
            _colors = new Color[Game.Graphics.PreferredBackBufferHeight/16/2];
        }

        public static void Toggle(TimeSpan totalGameTime)
        {
            if (_lastActive + 0.5 > totalGameTime.TotalSeconds) return;
            _lastActive = totalGameTime.TotalSeconds;
            Active = !Active;

            if (Active) Game.ActiveState.DoDisable(-1);
            else Game.ActiveState.DoEnable(-1);
        }

        public static void Update()
        {
            var keys = Game.JustKeysDown();

            if (keys.Count == 0) return;

            if (keys.Contains(Keys.Up) & CommandHistory.Count > 0)
            {
                _historyList++;
                if (_historyList > CommandHistory.Count - 1) _historyList = CommandHistory.Count - 1;
                _inputLine = CommandHistory[CommandHistory.Count - 1 - _historyList];
            }

            if (keys.Contains(Keys.Down) & CommandHistory.Count > 0)
            {
                _historyList--;
                if (_historyList == -1)
                {
                    _inputLine = "";
                    return;
                }
                if (_historyList < -1)
                {
                    _historyList = -1;
                    return;
                }

                _inputLine = CommandHistory[CommandHistory.Count - 1 - _historyList];
            }

            if (keys.Contains(Keys.Back))
            {
                if (_inputLine.Length == 0) return;
                _inputLine = _inputLine.Substring(0, _inputLine.Length - 1);
                return;
            }

            if (keys.Contains(Keys.Space))
            {
                _inputLine += " ";
                return;
            }
            if (keys.Contains(Keys.Enter))
            {
                if (_inputLine.Length == 0) return;
                WriteLine(_inputLine);

                var commandArgs = (ConsoleCommandArgs) _inputLine;
                CommandHistory.Add(_inputLine);
                _historyList = -1;
                _inputLine = "";

                foreach (
                    var command in Commands.Where(command => command.Check(commandArgs) == CommandCheckReturnCode.Match)
                    )
                {
                    var returnCode = command.Execute(commandArgs);
                    switch (returnCode)
                    {
                        case CommandReturnCode.GeneralError:
                            WriteLine("Error");
                            break;
                        case CommandReturnCode.MissingArguments:
                            WriteLine("Usage: " + command.Name + " " + command.Usage);
                            break;
                        case CommandReturnCode.InvalidArgument:
                            WriteLine("Invalid arguments");
                            break;
                        case CommandReturnCode.FailedQuery:
                            WriteLine("Object not found");
                            break;
                    }

                    return;
                }

                WriteLine("Unknown command!");
            }

            for (var i = 0; i < keys.Count; i++)
            {
                if (keys[i].ToString().Length > 1) keys.Remove(keys[i]);
            }

            if (keys.Count <= 0) return;

            if (keys[0].ToString().Length > 1) return;

            if (Game.KState.IsKeyDown(Keys.LeftShift) | Game.KState.IsKeyDown(Keys.RightShift))
                _inputLine += keys[0].ToString().ToUpper();
            else _inputLine += keys[0].ToString().ToLower();
        }

        public static void WriteLine(string text)
        {
            WriteLine(text, Color.Black);
        }

        public static void WriteLine(string text, Color color)
        {
            var textLine = text.Split('\n');
            foreach (var t in textLine)
            {
                var written = false;

                for (var i = 0; i < _lines.Length; i++)
                {
                    if (_lines[i] == null)
                    {
                        _lines[i] = t;
                        _colors[i] = color;
                        written = true;
                        break;
                    }
                }

                if (written) continue;

                for (var i = 1; i < _lines.Length; i++)
                {
                    _lines[i - 1] = _lines[i];
                    _colors[i - 1] = _colors[i];
                }

                _lines[_lines.Length - 1] = t;
                _colors[_lines.Length - 1] = color;
            }
        }

        public static void Draw()
        {
            if (!Active) return;

            Game.SpriteBatch.Begin();


            var height =
                (int)
                    ((16 - ((float) Game.Graphics.PreferredBackBufferHeight/2%16)) +
                     ((float) Game.Graphics.PreferredBackBufferHeight/2));
            Game.SpriteBatch.Draw(ConsoleBckg, new Vector2(0, 0), null, Color.White, 0, new Vector2(0, 0),
                new Vector2(Game.Graphics.PreferredBackBufferWidth, height + 5), SpriteEffects.None,
                0);

            for (var i = 0; i < _lines.Length; i++)
            {
                if (_lines[i] == null) break;
                Game.SpriteBatch.DrawString(ConsoleFont, _lines[i], new Vector2(5, i*16), _colors[i]);
            }

            Game.SpriteBatch.DrawString(ConsoleFont, _inputLine, new Vector2(5, height - 16), Color.Black);

            Game.SpriteBatch.End();
        }
    }
}