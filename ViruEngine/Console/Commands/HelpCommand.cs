﻿namespace ViruEngine.Console.Commands
{
    internal class HelpCommand : ConsoleCommand
    {
        public HelpCommand(ViruGame game) : base(game)
        {
            Name = "help";
            Description = "Lists all commands";
            Usage = "";
        }

        public override CommandReturnCode Execute(ConsoleCommandArgs msgArgs)
        {
            foreach (var command in GameConsole.Commands)
            {
                GameConsole.WriteLine(command.Name + " " + command.Usage + ": " + command.Description);
            }

            return CommandReturnCode.Success;
        }
    }
}