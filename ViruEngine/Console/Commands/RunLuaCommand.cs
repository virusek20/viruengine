﻿namespace ViruEngine.Console.Commands
{
    class RunLuaCommand : ConsoleCommand
    {
        public RunLuaCommand(ViruGame game) : base(game)
        {
            Name = "runLua";
            Description = "Runs argument as a Lua script";
            Usage = "\"code\"";
        }

        public override CommandReturnCode Execute(ConsoleCommandArgs msgArgs)
        {
            if (msgArgs.Arguments.Count != 2) return CommandReturnCode.MissingArguments;

            try
            {
                GameConsole.Lua.DoString(msgArgs.Arguments[1]);

            }
            catch
            {
                return CommandReturnCode.GeneralError;
            }

            return CommandReturnCode.Success;
        }
    }
}
