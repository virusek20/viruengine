﻿namespace ViruEngine.Console.Commands
{
    internal class ExitCommand : ConsoleCommand
    {
        public ExitCommand(ViruGame game) : base(game)
        {
            Name = "exit";
            Description = "Exits game";
            Usage = "";
        }

        public override CommandReturnCode Execute(ConsoleCommandArgs msgArgs)
        {
            Game.Exit();
            return CommandReturnCode.Success;
        }
    }
}