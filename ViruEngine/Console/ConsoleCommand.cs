﻿namespace ViruEngine.Console
{
    public enum CommandReturnCode
    {
        Success,
        MissingArguments,
        GeneralError,
        Failure,
        InvalidArgument,
        FailedQuery
    }

    public enum CommandCheckReturnCode
    {
        Different,
        Match
    }

    public abstract class ConsoleCommand
    {
        public string Description;
        protected ViruGame Game;
        public string Name;
        public string Usage;

        protected ConsoleCommand(ViruGame game)
        {
            Game = game;
        }

        public virtual CommandCheckReturnCode Check(ConsoleCommandArgs msgArgs)
        {
            return msgArgs.Arguments[0].ToLower() == Name.ToLower()
                ? CommandCheckReturnCode.Match
                : CommandCheckReturnCode.Different;
        }

        public virtual CommandReturnCode Execute(ConsoleCommandArgs msgArgs)
        {
            return CommandReturnCode.GeneralError;
        }
    }
}