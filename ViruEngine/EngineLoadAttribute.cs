﻿using System;

namespace ViruEngine
{
    internal enum LoadTime
    {
        PreInit,
        Init,
        LoadContent
    }

    internal class EngineLoadAttribute : Attribute
    {
        public EngineLoadAttribute(bool passGame, LoadTime loadTime)
        {
            PassGame = passGame;
            LoadTime = loadTime;
        }

        public bool PassGame { get; private set; }
        public LoadTime LoadTime { get; private set; }
    }
}