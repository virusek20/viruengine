using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using ViruEngine.Console;
using ViruEngine.Entities;
using ViruEngine.GameStates;
using ViruEngine.Graphics.Textures;
using ViruEngine.GUI;
using ViruEngine.PersistentData;

namespace ViruEngine
{
    public class ViruGame : Game
    {
        private GameState _activeState;
        public SpriteFont BasicFont;
        public string GameName;
        public GraphicsDeviceManager Graphics;
        public KeyboardState KState;
        public KeyboardState LastKState;
        public MouseState LastMState;
        public RenderTarget2D MainRenderTarget;
        public MouseState MState;
        public SpriteBatch SpriteBatch;
        public Transition TransitionState = new Transition();
        public TexturedButton WarningIcon;
        public Random Random = new Random();

        public ViruGame(string gameName)
        {
            GameName = gameName;

            Graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            Window.Title = gameName;
            IsMouseVisible = true;

            Settings.Load();
        }

        public GameState ActiveState
        {
            get { return _activeState; }
            set
            {
                if (value != null) GameConsole.WriteLine("Switching gamestate to: " + value);
                else GameConsole.WriteLine("Switching gamestate to: ViruEngine.GameStates.NullState");
                if (_activeState != null)
                {
                    if (_activeState.ContentLoaded) _activeState.UnloadContent();
                }
                _activeState = value ?? new NullState();

                WarningIcon = new TexturedButton(this, _activeState, "WarningIcon")
                {
                    Color = Color.White,
                    ConsoleColor = Color.White,
                    OnResize = () => new Vector2(Graphics.PreferredBackBufferWidth - 100, 32),
                    Priority = 4,
                    Visibility = HiddenState.Hidden,
                    OverColor = Color.White,
                    Texture = ImageTexture.FromImage("icon_warning.png"),
                    TextureSize = 64,
                    Position = new Vector2(Graphics.PreferredBackBufferWidth - 100, 32),
                    AutoClose = 3
                };


                if (!_activeState.Loaded) _activeState.Initialize(this);
                if (!_activeState.ContentLoaded) _activeState.LoadContent(Content);
            }
        }

        public void LoadFont()
        {
            BasicFont = Content.Load<SpriteFont>("Fonts/Arial");
        }

        public bool JustPressed(bool leftMouse = true)
        {
            if (leftMouse)
                return LastMState.LeftButton == ButtonState.Released & MState.LeftButton == ButtonState.Pressed;

            return LastMState.RightButton == ButtonState.Released & MState.RightButton == ButtonState.Pressed;
        }

        public bool JustKeyDown(Keys key)
        {
            return LastKState.IsKeyDown(key) == false & KState.IsKeyDown(key);
        }

        public List<Keys> JustKeysDown()
        {
            return KState.GetPressedKeys().Where(keyState => LastKState.IsKeyUp(keyState)).ToList();
        }

        private void InitializeClasses(Type[] types, LoadTime loadTime)
        {
            foreach (var type in types)
            {
                var initMethod = type.GetMethod("Initialize");
                if (initMethod == null) continue;
                if (!initMethod.IsStatic) continue;
                var loadAttributes = initMethod.GetCustomAttributes(typeof (EngineLoadAttribute), false);
                if (loadAttributes.Length == 0) continue;
                var loadAttribute = loadAttributes[0] as EngineLoadAttribute;
                if (loadAttribute == null) continue;
                if (loadAttribute.LoadTime != loadTime) continue;

                initMethod.Invoke(null, loadAttribute.PassGame ? new object[] {this} : null);
            }
        }

        protected override void Initialize()
        {
            MainRenderTarget = new RenderTarget2D(GraphicsDevice, Graphics.PreferredBackBufferWidth,
                Graphics.PreferredBackBufferHeight, false, SurfaceFormat.Color, DepthFormat.None, 0,
                RenderTargetUsage.PreserveContents);
            LoadFont();

            TransitionState.Initialize(this);

            InitializeClasses(Assembly.GetExecutingAssembly().GetTypes(), LoadTime.PreInit);
            InitializeClasses(Assembly.GetEntryAssembly().GetTypes(), LoadTime.PreInit);

            InitializeClasses(Assembly.GetExecutingAssembly().GetTypes(), LoadTime.Init);
            InitializeClasses(Assembly.GetEntryAssembly().GetTypes(), LoadTime.Init);

            Exiting += delegate
            {
                ActiveState.UnloadContent();
                Settings.Save();
            };

            SoundEffect.MasterVolume = Settings.Volume;

            GameConsole.WriteLine(GameName + " " + Assembly.GetExecutingAssembly().GetName().Version + " init");

            var entityDefinitionType =
                Assembly.GetEntryAssembly().GetType(Assembly.GetEntryAssembly().GetName().Name + ".EntityDefinitions");
            if (entityDefinitionType != null)
            {
                foreach (var field in entityDefinitionType.GetFields())
                {
                    EntityManager.Register(field.Name, field.GetValue(null) as EntityManager.GameEntityCreationDelegate);
                }

                GameConsole.WriteLine("Automatically loaded " + entityDefinitionType.GetFields().Count() + " entities");
            }
            else
            {
                GameConsole.WriteLine("No entity definitions found!", Color.Red);
            }

            if (_activeState == null) ActiveState = new NullState();

            base.Initialize();
        }

        protected override void LoadContent()
        {
            SpriteBatch = new SpriteBatch(GraphicsDevice);

            InitializeClasses(Assembly.GetExecutingAssembly().GetTypes(), LoadTime.LoadContent);
            InitializeClasses(Assembly.GetEntryAssembly().GetTypes(), LoadTime.LoadContent);
        }

        protected override void Update(GameTime gameTime)
        {
            if (!IsActive) return;

            var kState = Keyboard.GetState();

            LastMState = MState;
            MState = Mouse.GetState();

            LastKState = KState;
            KState = Keyboard.GetState();

            if (kState.IsKeyDown(Keys.OemTilde))
            {
                GameConsole.Toggle(gameTime.TotalGameTime);
            }

            if (GameConsole.Active)
            {
                GameConsole.Update();
            }
            else
            {
                _activeState.Update(gameTime);
            }

            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.SetRenderTarget(MainRenderTarget);

            GraphicsDevice.Clear(Color.Black);

            _activeState.Draw(gameTime);
            GameConsole.Draw();

            GraphicsDevice.SetRenderTarget(null);

            SpriteBatch.Begin();
                SpriteBatch.Draw(MainRenderTarget, Vector2.Zero, Color.White);
            SpriteBatch.End();

            base.Draw(gameTime);
        }
    }
}