﻿using System.Collections.Generic;
using ViruEngine.World;

namespace ViruEngine.Entities
{
    public static class EntityManager
    {
        public delegate GameEntity GameEntityCreationDelegate(ViruGame game, GameLevel level);

        public static readonly Dictionary<string, GameEntityCreationDelegate> Presets =
            new Dictionary<string, GameEntityCreationDelegate>();

        public static void Register(string entityName, GameEntityCreationDelegate gameEntityCreationDelegate)
        {
            Presets.Add(entityName, gameEntityCreationDelegate);
        }

        public static GameEntity BuildEntity(string entityName, ViruGame game, GameLevel level)
        {
            var entity = Presets.ContainsKey(entityName) ? Presets[entityName](game, level) : null;
            if (entity == null) return null;

            entity.Level = level;
            entity.Game = game;

            return entity;
        }
    }
}