﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using ViruEngine.ComponentSystem;
using ViruEngine.Graphics.Textures;
using ViruEngine.World;

namespace ViruEngine.Entities
{
    public class GameEntity : ComponentObject<Component<GameEntity>>
    {
        private bool _spawned;
        public string ID;
        protected bool Initialized;
        public GameLevel Level;
        public Vector2 Position = Vector2.Zero;
        public AnimatedTexture Texture;
        public int UpdateOrder;

        public GameEntity(ViruGame game, GameLevel level)
        {
            Game = game;
            Level = level;
        }

        public bool GetSpawnedState()
        {
            return _spawned;
        }

        public void SetSpawnedState(GameTime gameTime, bool spawnedState)
        {
            if (_spawned == spawnedState) return;
            if (spawnedState) Init(gameTime);
            else Deinit(gameTime);

            _spawned = spawnedState;
        }

        public void Update(GameTime gameTime)
        {
            if (!GetSpawnedState()) return;

            foreach (var component in ComponentList.Values)
            {
                component.Update(gameTime);
            }
        }

        public void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            if (!GetSpawnedState()) return;

            foreach (var component in ComponentList.Values)
            {
                component.Draw(gameTime, spriteBatch);
            }

            if (Texture != null) Texture.Draw(Position);
        }

        public void Init(GameTime gameTime)
        {
            if (Initialized) return;

            foreach (var component in ComponentList.Values)
            {
                component.Init(gameTime);
            }

            Initialized = true;
        }

        public void Deinit(GameTime gameTime)
        {
            if (!Initialized) return;

            foreach (var component in ComponentList.Values)
            {
                component.Deinit(gameTime);
            }

            Initialized = false;
        }
    }
}