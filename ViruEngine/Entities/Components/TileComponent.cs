﻿using Microsoft.Xna.Framework;
using ViruEngine.ComponentSystem;
using ViruEngine.Physics;

namespace ViruEngine.Entities.Components
{
    public class TileComponent : Component<GameEntity>
    {
        public static Vector2 TileSize = Vector2.Zero;
        public Polygon CollisionBox;
        public Vector2 TilePosition = Vector2.Zero;

        public TileComponent(ViruGame game, GameEntity parent)
            : base(game, parent)
        {
        }

        public override void Init(GameTime gameTime)
        {
            Parent.Position = ResolvePosition();
        }

        public Vector2 ResolvePosition()
        {            return Vector2.Multiply(TilePosition, TileSize);
        }
    }
}