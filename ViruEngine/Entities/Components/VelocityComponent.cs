﻿using System;
using Microsoft.Xna.Framework;
using ViruEngine.ComponentSystem;

namespace ViruEngine.Entities.Components
{
    public class VelocityComponent : Component<GameEntity>
    {
        public Vector2 Acceleration = new Vector2(2, 2);
        public Vector2 Friction = new Vector2(0, 0);
        public Vector2 TerminalVelocity = new Vector2(5, 5);
        public Vector2 Velocity = new Vector2(0, 0);
        public bool GravityMode = false;

        public VelocityComponent(ViruGame game, GameEntity parent) : base(game, parent)
        {
        }

        public override void Update(GameTime gameTime)
        {
            Parent.Position += Velocity;

            if (Velocity.X > 0) Velocity.X -= Friction.X;
            else if (Velocity.X < 0 & !GravityMode) Velocity.X += Friction.X;

            if (Velocity.Y < 0) Velocity.Y += Friction.Y;
            if (Velocity.Y > 0) Velocity.Y -= Friction.Y;

            if (Math.Abs(Velocity.X) < Friction.X) Velocity.X = 0;
            if (Math.Abs(Velocity.Y) < Friction.Y) Velocity.Y = 0;

            if (Math.Abs(Velocity.X) > TerminalVelocity.X) Velocity.X = TerminalVelocity.X*Math.Sign(Velocity.X);
            if (Math.Abs(Velocity.Y) > TerminalVelocity.Y) Velocity.Y = TerminalVelocity.Y*Math.Sign(Velocity.Y);
        }
    }
}