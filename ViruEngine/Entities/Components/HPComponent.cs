﻿using System;
using System.IO;
using Microsoft.Xna.Framework;
using ViruEngine.ComponentSystem;

namespace ViruEngine.Entities.Components
{
    public class HPComponent : Component<GameEntity>
    {
        public float HP = 100;
        public float MaxHP = 100;

        public HPComponent(ViruGame game, GameEntity parent) : base(game, parent)
        {
        }

        public override void Save(BinaryWriter writer)
        {
            base.Save(writer);

            writer.Write(HP);
            writer.Write(MaxHP);
        }

        public override void Load(BinaryReader reader)
        {
            var dataSize = reader.ReadInt32();

            if (dataSize != GetSaveSize())
            {
                throw new Exception(GetType().Name + ": saved data size doesnt match (" + GetSaveSize() + "/" + dataSize +
                                    ")");
            }

            HP = reader.ReadSingle();
            MaxHP = reader.ReadSingle();
        }

        public override int GetSaveSize()
        {
            return sizeof (int)*2;
        }

        public override void Update(GameTime gameTime)
        {
            if (HP > MaxHP) HP = MaxHP;
            if (HP <= 0) Parent.Deinit(gameTime);
        }
    }
}