﻿using Microsoft.Xna.Framework;
using ViruEngine.ComponentSystem;

namespace ViruEngine.Entities.Components
{
    public delegate void InteractDelegate(GameTime gameTime, GameEntity interactor);

    public class InteractComponent : Component<GameEntity>
    {
        public InteractDelegate OnInteract = (time, interacter) => { };

        public InteractComponent(ViruGame game, GameEntity parent) : base(game, parent)
        {
        }
    }
}