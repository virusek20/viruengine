﻿using Microsoft.Xna.Framework;
using ViruEngine.ComponentSystem;
using ViruEngine.Physics;
using ViruEngine.World.Components;

namespace ViruEngine.Entities.Components
{
    public class CollisionComponent : Component<GameEntity>
    {
        public delegate void OnCollisionDelegate(GameTime gameTime, GameEntity collider, GameEntity worldCollider,
            Collision.PolygonCollisionResult result);

        public Polygon CollisionBox = new Polygon();
        public Collision.PolygonCollisionResult LastCollisionResult;
        public bool LeanLeft;
        public bool LeanRight;
        public bool OnGround;
        public OnCollisionDelegate OnCollision = (time, collider, worldCollider, result) => { };

        public CollisionComponent(ViruGame game, GameEntity parent) : base(game, parent)
        {
        }

        public override bool CheckAvailability(GameEntity gameEntity)
        {
            return gameEntity.HasComponent<VelocityComponent>();
        }

        public override void Update(GameTime gameTime)
        {
            CollisionBox.X = (int) Parent.Position.X;
            CollisionBox.Y = (int) Parent.Position.Y;

            if (!OnGround & Parent.Level.HasComponent<GravityComponent>())
                Parent.GetComponent<VelocityComponent>().Velocity.Y +=
                    Parent.Level.GetComponent<GravityComponent>().Gravity;
        }

        public void CollideWorld(GameTime gameTime, GameEntity collider, GameEntity worldCollider,
            Collision.PolygonCollisionResult result)
        {
            if (result.Intersect | result.WillIntersect) LastCollisionResult = result;
            if (!result.WillIntersect & !result.Intersect) return;

            //System.Console.WriteLine("------");
            //System.Console.WriteLine(gameTime.TotalGameTime);
            //System.Console.WriteLine(result.MinimumTranslationVector + ":" + result.Intersect);
            //System.Console.WriteLine(collider.GetComponent<CollisionComponent>().CollisionBox.ToString());
            //System.Console.WriteLine(worldCollider.GetComponent<TileComponent>().CollisionBox.ToString());

            if (result.WillIntersect)
            {
                collider.GetComponent<VelocityComponent>().Velocity.X += result.MinimumTranslationVector.X;
                collider.GetComponent<VelocityComponent>().Velocity.Y += result.MinimumTranslationVector.Y;
            }
            else
            {
                collider.GetComponent<VelocityComponent>().Velocity = Vector2.Zero;

                collider.Position.X += result.MinimumTranslationVector.X;
                collider.Position.Y += result.MinimumTranslationVector.Y;
            }


            if (result.Intersect & result.MinimumTranslationVector.Y == 0 & result.MinimumTranslationVector.X == 0)
                OnGround = true;

            OnCollision(gameTime, collider, worldCollider, result);
        }

        public void CollideEntity(GameTime gameTime, GameEntity collider, GameEntity collider2, Collision.PolygonCollisionResult result, bool moveEntities)
        {
            if (result.Intersect | result.WillIntersect) LastCollisionResult = result;
            if (!result.WillIntersect & !result.Intersect) return;

            if (result.WillIntersect)
            {
                collider.GetComponent<VelocityComponent>().Velocity.X += result.MinimumTranslationVector.X;
                collider.GetComponent<VelocityComponent>().Velocity.Y += result.MinimumTranslationVector.Y;
            }
            else
            {
                collider.GetComponent<VelocityComponent>().Velocity = Vector2.Zero;

                collider.Position.X += result.MinimumTranslationVector.X;
                collider.Position.Y += result.MinimumTranslationVector.Y;
            }

            OnCollision(gameTime, collider, collider2, result);
            collider2.GetComponent<CollisionComponent>().OnCollision(gameTime, collider, collider2, result);
        }
    }
}